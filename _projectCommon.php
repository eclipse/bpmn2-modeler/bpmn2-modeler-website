<?php

/*******************************************************************************
 * Copyright (c) 2009-2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	# Set the theme for your project's web pages.
	# See http://eclipse.org/phoenix/
	$theme = "Nova";
	

	# Define your project-wide Navigation here
	# This appears on the left of the page if you define a left nav
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# these are optional
	
	# If you want to override the eclipse.org navigation, uncomment below.
	# $Nav->setLinkList(array());
	
	# Break the navigation into sections
	$Nav->addNavSeparator("BPMN2 Modeler", 	"/bpmn2-modeler");
   	$Nav->addCustomNav("Download", "/bpmn2-modeler/downloads.php", "_self", 3);
	$Nav->addCustomNav("Documentation", "/bpmn2-modeler/documentation.php", "_self", 3);
	$Nav->addCustomNav("Support", "/bpmn2-modeler/support.php", "_self", 3);
	$Nav->addCustomNav("Getting Involved", "/bpmn2-modeler/developers.php", "_self", 3);

	# Define keywords, author and title here, or in each PHP page specifically
	$pageKeywords	= "eclipse, BPMN2, BPM, Modeling, business process";
	$pageAuthor		= "Bob Brodt";

	# top navigation bar
	# To override and replace the navigation with your own, uncomment the line below.
	$Menu->setMenuItemList(array());
	$Menu->addMenuItem("Eclipse", "/", "_self");
	$Menu->addMenuItem("BPMN2 Modeler", "/bpmn2-modeler", "_self");
	$Menu->addMenuItem("Download", "/bpmn2-modeler/downloads.php", "_self");
	$Menu->addMenuItem("Documentation", "/bpmn2-modeler/documentation.php", "_self");
	$Menu->addMenuItem("Support", "/bpmn2-modeler/support.php", "_self");
	$Menu->addMenuItem("Developers", "/bpmn2-modeler/developers.php", "_self");
    $Menu->addMenuItem("About", "http://www.eclipse.org/projects/project_summary.php?projectid=soa.bpmn2-modeler", "_self");
	
	# To define additional CSS or other pre-body headers
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/bpmn2-modeler/style.css"/>');
	
	# To enable occasional Eclipse Foundation Promotion banners on your pages (EclipseCon, etc)
	$App->Promotion = TRUE;
	
	# If you have Google Analytics code, use it here
	# $App->SetGoogleAnalyticsTrackingCode("YOUR_CODE");
?>