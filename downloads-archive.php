<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2009 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - Downloads";

	$html  = <<<EOHTML
<div id="midcolumn">
<h2>$pageTitle</h2>
<p>All downloads are provided under the terms and conditions of the <a href="/legal/epl/notice.php">Eclipse Foundation Software User Agreement</a> unless otherwise specified.</p>

<h3>Eclipse 4.4 (Luna)</h3>
<p><b>Nightly Build</b><br />		
<p><a href="http://download.eclipse.org/bpmn2-modeler/updates/nightly/luna/">http://download.eclipse.org/bpmn2-modeler/updates/nightly/luna/</a>
<p><b>Update Sites</b><br />		
<p><b>1.1.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.0">http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.0</a>
<p><b>1.1.1:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.1">http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.1</a>
<p><b>1.1.2:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.2">http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.2</a>
<p><b>1.1.3:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.3">http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.3</a>
<p><b>1.1.4:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.4">http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.4</a>
<p><b>1.1.5:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.5">http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.5</a>
<p><b>1.1.6:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.6">http://download.eclipse.org/bpmn2-modeler/updates/luna/1.1.6</a>
		
<h3>Eclipse 3.6 (Helios)</h3>
<p><b>Nightly Build</b><br />		
<p><a href="http://download.eclipse.org/bpmn2-modeler/updates/nightly/helios/">http://download.eclipse.org/bpmn2-modeler/updates/nightly/helios/</a>
<p><b>Update Sites</b><br />		
<p><b>0.1.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/helios/0.1.0">http://download.eclipse.org/bpmn2-modeler/updates/helios/0.1.0</a>
<p><b>0.2.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.0">http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.0</a>
<p><b>0.2.1:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.1">http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.1</a>
<p><b>0.2.2:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.2">http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.2</a>
<p><b>0.2.3:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.3">http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.3</a>
<p><b>0.2.4:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.4">http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.4</a>
<p><b>0.2.5:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.5">http://download.eclipse.org/bpmn2-modeler/updates/helios/0.2.5</a>
		
<h3>Eclipse 3.7 - 4.2 (Indigo, Juno)</h3>
<p><b>Nightly Build</b><br />		
<p><a href="http://download.eclipse.org/bpmn2-modeler/updates/nightly/juno/">http://download.eclipse.org/bpmn2-modeler/updates/nightly/juno/</a>
<p><b>Update Sites</b><br />		
<p><b>0.1.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/juno/0.1.0">http://download.eclipse.org/bpmn2-modeler/updates/juno/0.1.0</a>
<p><b>0.2.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.0">http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.0</a>
<p><b>0.2.1:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.1">http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.1</a>
<p><b>0.2.2:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.2">http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.2</a>
<p><b>0.2.3:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.3">http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.3</a>
<p><b>0.2.4:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.4">http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.4</a>
<p><b>0.2.5:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.5">http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.5</a>
<p><b>0.2.6:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.6">http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.6</a>
<p><b>0.2.7:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.7">http://download.eclipse.org/bpmn2-modeler/updates/juno/0.2.7</a>

		
<h3>Eclipse 4.3 (Kepler)</h3>
<p><b>Nightly Build</b><br />		
<p><a href="http://download.eclipse.org/bpmn2-modeler/updates/nightly/kepler/">http://download.eclipse.org/bpmn2-modeler/updates/nightly/kepler/</a>
<p><b>Update Sites</b><br />		
<p><b>0.1.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.1.0">http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.1.0</a>
<p><b>0.2.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.0">http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.0</a>
<p><b>0.2.1:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.1">http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.1</a>
<p><b>0.2.2:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.2">http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.2</a>
<p><b>0.2.3:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.3">http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.3</a>
<p><b>0.2.4:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.4">http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.4</a>
<p><b>0.2.5:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.5">http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.5</a>
<p><b>0.2.6:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.6">http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.6</a>
<p><b>0.2.7:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.7">http://download.eclipse.org/bpmn2-modeler/updates/kepler/0.2.7</a>
<p><b>1.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/1.0">http://download.eclipse.org/bpmn2-modeler/updates/kepler/1.0</a>
<p><b>1.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/1.0.1">http://download.eclipse.org/bpmn2-modeler/updates/kepler/1.0.1</a>

<p><b>Zip Files</b><br />		
<p><b>1.0.1:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/kepler/1.0.1/site_assembly.zip">http://download.eclipse.org/bpmn2-modeler/updates/kepler/1.0.1/site_assembly.zip</a>


</div>
EOHTML;
	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>