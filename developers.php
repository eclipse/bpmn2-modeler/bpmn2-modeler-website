<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - Getting Involved";

	$html  = <<<EOHTML
<div id="midcolumn">
<h2>$pageTitle</h2>
<ul>
	<li>
	    <a href="http://wiki.eclipse.org/BPMN2-Modeler">Wiki</a><br>Source of all knowledge
	</li>
	<li>
		<a href="https://git.eclipse.org/c/bpmn2-modeler/org.eclipse.bpmn2-modeler.git">Git Repository</a>
		<br>The primary Git repository for BPMN2 Modeler. This repository is also mirrored at
		<a href="https://github.com/eclipse/bpmn2-modeler">github</a> where you can submit your pull requests.
	</li>
	<li>
		<a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=SOA&list_id=1567033&product=BPMN2Modeler">Contribute</a>
		<br>If you have a few spare cycles and are interested in helping out, pick from
		our current list of open bugs and send us your contributions.
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>