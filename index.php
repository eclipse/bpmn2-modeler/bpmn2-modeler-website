<?php
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *
 *******************************************************************************/
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App = new App ();
$Nav = new Nav ();
$Menu = new Menu ();
include ($App->getProjectCommon ());

$localVersion = false;

$pageTitle = "Eclipse BPMN2 Modeler";

// # Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML



<div id="bigbuttons-container">
	<div id="bigbuttons">
	<h3>Primary Links</h3>
	<ul>
	<li><a id="buttonDownload" href="downloads.php" title="Download">
		BPMN2 Modeler Software Install Site</a></li>
	<li><a id="buttonDocumentation" href="documentation.php" title="Documentation">
		Tutorials, Examples, Videos, Reference Documentation</a></li>
	<li><a id="buttonSupport" href="support.php" title="Support">
		Bug Tracker, Newsgroup</a></li>
	<li><a id="buttonInvolved" href="developers.php" title="Getting Involved">
		CVS, Workspace Setup, Wiki, Committers</a></li>
	</ul>
	</div>
</div>

<div id="midcolumn">

<div id="introText">

<h1>BPMN2 Modeler</h1>

		<p>
		The Eclipse BPMN2 Modeler is a graphical modeling tool for authoring business processes.
		The primary goal of BPMN2 Modeler was to provide a graphical workflow editing framework,
		which can be easily customized for any BPMN 2.0 compliant execution engine.
		</p>
		
<h1>Background</h1>
		<p>
		<a href="http://www.omg.org/spec/BPMN/2.0/">Business Process Model and Notation</a>
		is an XML language which was proposed by the
		<a href="http://www.omg.org">Object Management Group</a> (OMG), as a notation for describing
		not only business workflows (a.k.a. "processes")
		but also higher-level collaborations between internal or external business partners and the
		choreography of information exchanged between these business partners.
		</p>
		<p>
		The BPMN language was intended for users at all levels, from the business analysts who
		create the initial design, to the developers who implement the technical details,
		and finally, to the business users responsible for managing and monitoring the processes.
		</p>
		<p>
		BPMN 2.0 has evolved to become a complex specification which tries to be all things to all people involved
		in the design of business processes.
		Because of this complexity, writing BPMN XML and visualizing business processes
		becomes nearly impossible without the use of a graphical tool.
		BPMN2 Modeler fills this need nicely by providing an intuitive user interface.
		It interprets the complexities of the BPMN language in the form of drawing canvases,
		tool palettes, property sheets and other familiar UI elements. 
		</p>
		
		<img class="displayed" src="/bpmn2-modeler/images/snapshots.gif" width="632" alt="BPMN2 Modeler Snapshots" border="0"/>
		
<h1>Extensibility</h1>
		<p>
		BPMN 2.0 introduces an extensibility mechanism that allows extending standard BPMN elements with additional attributes.
		It can be used by modelers and modeling tools to add non-standard elements or Artifacts to satisfy a specific need,
		such as the unique requirements of a vertical domain, and still have valid BPMN Core. 
		</p>
		<p>
		The goal of the Eclipse BPMN2 Modeler is to not only provide a graphical modeling tool,
		but also to allow plug-in developers to easily customize the behavior and appearance of the editor for specific BPM workflow engines
		that use this BPMN 2.0 extensibility mechanism.
		</p>
		<p>
		Open Source Workflow Engines like <a href="https://www.jbpm.org/" target="_blank">jBPM</a> and <a href="https://www.imixs.org" target="_blank">Imixs-Workflow</a>
		integrate Eclipse BPMN2 into there tooling platforms and extend the core features of BPMN2 with platform specific functionality. 
		Learn more about this projects on the wiki page <a href="https://wiki.eclipse.org/BPMN2-Modeler/Open_Source_Workflow_Engines">Open Source Workflow Engines</a>. If you plan to implement you own BPMN2 extensions and 
		learn more about the basics of Eclipse BPMN2 Runtime extensions, read 
		the <a href="https://wiki.eclipse.org/BPMN2-Modeler/DeveloperTutorials/ExtendingRuntime">Wiki Developer Tutorial</a>.

<h1>Architecture</h1>
		<p>
		The BPMN2 Modeler is built on the <a href="http://www.eclipse.org/articles/Article-Plug-in-architecture/plugin_architecture.html">Eclipse Plug-in Architecture</a>
		and provides several extension points for customizing the editor's appearance and behavior.
		The <a href="https://wiki.eclipse.org/BPMN2-Modeler/DeveloperTutorials">Deveoper Tutorials Wiki pages</a> provide several examples and guidelines
		for extending the capabilities of Eclipse BPMN2 Modeler.
		</p>
		<p>
		The foundation of the BPMN2 Modeler is the <a href="http://www.eclipse.org/modeling/mdt/?project=bpmn2">Eclipse BPMN 2.0 EMF meta model</a>
		which is part of the <a href="http://www.eclipse.org/modeling/mdt/">Model Development Tools</a> (MDT) project.
		The model was originally developed by contributing members of the OMG BPMN 2.0 working group and is fully compliant with the spec.
		</p>

<h1>Getting Involved</h1>
		<p>
		Please visit our <a href="https://wiki.eclipse.org/BPMN2-Modeler">Wiki pages</a> or
		<a href="/bpmn2-modeler/developers.php">click here</a> for more information about how you can contribute to this project. 
		</p>
</div>

</div>

<div id="rightcolumn">

  <div>
    	<h3>Latest News...</h3>

    	<h4>April 2021</h4>
		<b>Nightly Build 1.5.3 available!</b>
		<p>
		Nightly Build 1.5.3 for Eclipse 2020-09 is now available
		<a href="http://download.eclipse.org/bpmn2-modeler/updates/nightly/2020-09/">here</a>.
		Find out what's new in the <a href="whatsnew/whatsnew-1.5.3.php">Relase Notes</a>.
		See also all updates in <a href="whatsnew/">New &amp; Noteworthy</a>
		           
		</p>

    	<h3>Current Status</h3>
    	<h4>June 2020</h4>
		<b>BPMN Modeling for Eclipse 2020-06</b>
		<p>
		Version 1.5.2 for Eclipse 2020-06 is now available
		<a href="http://download.eclipse.org/bpmn2-modeler/updates/2020-06/1.5.2/">here</a>.
		Find out what's new in the <a href="whatsnew/whatsnew-1.5.2.php">Relase Notes</a>.
		See also all updates in <a href="whatsnew/">New &amp; Noteworthy</a>
		           
		</p>
  </div>
  
  
</div>
EOHTML;

// Generate the web page
$App->generatePage ( $theme, $Menu, null, $pageAuthor, $pageKeywords, $pageTitle, $html );

?>
