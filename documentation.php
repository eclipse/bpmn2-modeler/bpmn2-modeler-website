<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - Documentation";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<ul>
	<li>
		<a href="whatsnew/index.php">New &amp; Noteworthy</a><br>
	</li>
	<li>
		<a href="https://projects.eclipse.org/projects/soa.bpmn2-modeler">Eclipse Release Documentation</a><br>
		Eclipse release review documentation and IP Logs for the latest releases.
	</li>
	<li>
		<a href="releases">Project Proposal</a><br>
		The initial Project Proposal for BPMN2 Modeler.
	</li>
	<li>
		<a href="http://eclipse.org/projects/project-plan.php?planurl=bpmn2-modeler/project-info/project-plan.xml">Project Plan</a>
		<br>
		What's next.
	</li>
	<li>
		<a href="http://www.screencast.com/users/BobBrodt/playlists/BPMN2%20Modeler%20Videos">Screencasts</a>
			<br>
		Screencasts showing various BPMN2 Modeler capabilities and features. 
	</li>
	<li>
		<a href="documentation/BPMN2ModelerUserGuide-1.0.1.pdf">BPMN2 Modeler User Guide v 1.0.1</a>
	</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
