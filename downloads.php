<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2009 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - Downloads";

	$html  = <<<EOHTML
<div id="midcolumn">
<h2>$pageTitle</h2>
<p>All downloads are provided under the terms and conditions of the <a href="/legal/epl/notice.php">Eclipse Foundation Software User Agreement</a> unless otherwise specified.</p>

<h3>Eclipse 4.17 (2020-09)</h3>
<p><b>Nightly Build</b><br />		
<p><a href="http://download.eclipse.org/bpmn2-modeler/updates/nightly/2020-09/">http://download.eclipse.org/bpmn2-modeler/updates/nightly/2020-09/</a>


<h3>Eclipse 4.16 (2020-06)</h3>
<p><b>Nightly Build</b><br />		
<p><a href="http://download.eclipse.org/bpmn2-modeler/updates/nightly/2020-06/">http://download.eclipse.org/bpmn2-modeler/updates/nightly/2020-06/</a>
<p><b>Update Sites</b><br />		
<p><b>1.5.2:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/2020-06/1.5.2">http://download.eclipse.org/bpmn2-modeler/updates/2020-06/1.5.2</a>


<h3>Eclipse 4.12 (2019-06)</h3>
<p><b>Update Sites</b><br />		
<p><b>1.5.2:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/2019-06/1.5.2">http://download.eclipse.org/bpmn2-modeler/updates/2019-06/1.5.2</a>
<p><b>1.5.1:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/2019-06/1.5.1">http://download.eclipse.org/bpmn2-modeler/updates/2019-06/1.5.1</a>


<h3>Eclipse 4.8 (Photon)</h3>
<p><b>Update Sites</b><br />		
<p><b>1.5.1:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/photon/1.5.1">http://download.eclipse.org/bpmn2-modeler/updates/photon/1.5.1</a>
<p><b>1.5.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/photon/1.5.0">http://download.eclipse.org/bpmn2-modeler/updates/photon/1.5.0</a>



<h3>Eclipse 4.7 (Oxygen)</h3>
<p><b>Update Sites</b><br />		
<p><b>1.4.3:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/oxygen/1.4.3">http://download.eclipse.org/bpmn2-modeler/updates/oxygen/1.4.3</a>
<p><b>1.4.2:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/oxygen/1.4.2">http://download.eclipse.org/bpmn2-modeler/updates/oxygen/1.4.2</a>
<p><b>1.4.1:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/oxygen/1.4.1">http://download.eclipse.org/bpmn2-modeler/updates/oxygen/1.4.1</a>
<p><b>1.4.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/oxygen/1.4.0">http://download.eclipse.org/bpmn2-modeler/updates/oxygen/1.4.0</a>

<h3>Eclipse 4.6 (Neon)</h3>
<p><b>Update Sites</b><br />		
<p><b>1.3.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/neon/1.3.0">http://download.eclipse.org/bpmn2-modeler/updates/neon/1.3.0</a>

<h3>Eclipse 4.5 (Mars)</h3>
<p><b>Update Sites</b><br />		
<p><b>1.2.4:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.4">http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.4</a>
<p><b>1.2.3:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.3">http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.3</a>
<p><b>1.2.2:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.2">http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.2</a>
<p><b>1.2.1:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.1">http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.1</a>
<p><b>1.2.0:</b> <a href="http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.0">http://download.eclipse.org/bpmn2-modeler/updates/mars/1.2.0</a>
	
<h3>Older versions</h3>
<p><a href="downloads-archive.php">Can be found here.</a>

</div>
EOHTML;
	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>