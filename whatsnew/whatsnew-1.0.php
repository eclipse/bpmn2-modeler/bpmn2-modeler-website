<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.0 - Project Graduation and 1.0 Release (November 6, 2013)</h4>
	<p>
	As of this release, BPMN2 Modeler will only support Graphiti 0.10.x. That means Eclipse Kepler and beyond - Juno and previous versions of Eclipse, which use an earlier version of Graphiti, will no longer be supported.
	This does not mean that the BPMN2 Modeler will not run on Juno and prior versions of Eclipse, it simply means that you must upgrade Graphiti from 0.9.x to 0.10.x or later in your Juno workbench. 
	</p>
	<p>
	The User Preferences have been reorganized and the editor's response to Preference changes have been improved - there should be no need to close and reopen the editor
	for changed Preferences to take effect.
	</p>
	<p>
	During testing, a <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=420947">critical bug</a> was found and has been resolved.
	</p>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
