<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 0.2.1 Bug Fixes (January 12, 2013)</h4>
	What's new in this version:
	<ul>
		<li>
			The WSIL file support has been refactored and is now a separate installable feature. This allows the 
			BPMN2 editor to be installed with minimal dependencies on other projects such as BPEL and WTP.
		</li>

		<li>
			The minimal JVM target runtime is now set to 1.6 in the plugin manifests. This was causing some folks 
			problems with installation.
		</li>
		<li>
			According to the BPMN2 spec, Associations may have a direction and may be drawn with optional 
			arrowheads to indicate a single or bi-directional association. These are now displayed correctly
			in the  editor.
		</li>
		<li>
			Although Associations don't have a "name" attribute, an extension plugin may want to extend this 
			element and have the name appear as a label on the diagram next to the Association connection line. The 
			editor has been enhanced to support this - see the sample runtime plugin for details of how this is 
			done.
		</li>
		<li>
			Keeping separate versions of the editor for backward compatibility with helios continues to be a 
			problem mostly because of a breaking API change in Graphiti and SWT in indigo and juno. Now that the 
			kepler release and Graphiti 0.10 is on the horizon, we anticipate additional build issues. To help 
			mitigate these issues, the build.xml ant script has been enhanced to generate eclipse platform-specific 
			dependencies for Graphiti, the BPMN2 metamodel and [possibly] others.
		</li>
		<li>
			The sample runtime plugin has been enhanced to demonstrate many of the new features. Thanks to the 
			community for helping to define the extension API.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=396899">Bug 396899</a> - Can't change event, gateway or data object names
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=395476">Bug 395476</a> - Data Objects dropped into a SubProcess are not displayed
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=397474">Bug 397474</a> - Allow plugin to set toolpalette drawer title for "Custom Tasks"
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=355632">Bug 355632</a> - Message nodes missing from message links in collaboration diagram
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=374299">Bug 374299</a> - Connection layout is inconsistent between editing sessions
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=355641">Bug 355641</a> - Message links between pools are anchored using central point of connected nodes
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=355678">Bug 355678</a> - Resized pool results in incorrect message flow start connection point
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=376484">Bug 376484</a> - Moving (arrow) label does not work as expected when not zoomed 100%
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=396517">Bug 396517</a> - Custom Tasks not appearing in BPMN2 Diagram Editor Palette
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
