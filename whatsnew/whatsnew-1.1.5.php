<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.1.5 - Service Release (February 26, 2015)</h4>
	<p>
	This release back-ports several critical bug fixes made in later (Mars and Neon) versions.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1309945">RH Bug 1309945</a> - Palette Does Not Recognize Entries in WID File
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1309950">RH Bug 1309950</a> - MetaData Tag Introduces Bad Namespace
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/JBTIS-471">RH Bug JBTIS-471</a> - Unify component versions
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488131">Bug 488131</a> - Select All + Delete causes NPE when Boundary Event is involved
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1288044">RH Bug 1288044</a> - Fix Property Form resize problems
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=483689">Bug 483689</a> - ScriptTasks allow only 255 characters
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=484071">Bug 484071</a> - Drag and drop of call activity is corrupting the sequence flow
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=485268">Bug 485268</a> - The editor directly fetches the dtd from internet without looking into the XLM catalog
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488219">Bug 488219</a> - 'I/O Parameters' tab not rendered correctly
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/JBPM-4860">RH Bug JBPM-4860</a> - Can not save file because "No targetNamespace defined"
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1141619">RH Bug 1141619</a> - Boundary event should not be attached to Script Task in JBDS for jBPM
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=486970">Bug 486970</a> - Only Group name should be exposed by Group Property page for jBPM
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1188592">RH Bug 1188592</a> - Mapping Message from Receive Task to process variable
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=393034">Bug 393034</a> - Map Interface to ServiceTask
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=461614">Bug 461614</a> - Unable to create/edit jBPM MetaData elements.
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1309953">RH Bug 1309953</a> - Missing Prompt for Service Task Pop Up. Property page redrawing has been improved by using a UIJob.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488503">Bug 488503</a> - ResourcePropertyTester NPE prevents proper validation
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488792">Bug 488792</a> - Tool Palette extension point not working as expected
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488717">Bug 488717</a> - wrong layout of property tabs in property Dialog - createBindings() method
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=464052">Bug 464052</a> - Adding a 3rd Pool element is not working correctly when Full-Profile is active
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488889">Bug 488889</a> - MultInstanceLoopCharacteristics input and output data objects are not handled properly for Tasks
		</li>

	</ul>
	<p>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
