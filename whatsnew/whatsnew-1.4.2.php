<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2017
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.4.2 Eclipse Oxygen Release (December 15, 2017)</h4>
	<p>
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=521770">Bug 521770</a> NPE - ReconnectBaseElementFeature.preReconnect
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489487">Bug 489487</a>  Missing type definition for DataItemsPropertySection in plugin.xml  
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=501318">Bug 501318</a> Overlapping Lanes resize bug on delete 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=521993">Bug 521993</a> NPE in CustomTaskDescriptor.getImageId 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=521998">Bug 521998</a> NPE in DIUtils.findBPMNEdge
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522014">Bug 522014</a> ClassCastException in BoundaryEventPositionHelper.canMoveTo
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522258">Bug 522258</a> ClassCastException in DefaultPasteBPMNElementFeature.copyConnection 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522262">Bug 522262</a> NullPointerException in ModelUtil.generateUndefinedID
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522324">Bug 522324</a> ClassCastException in Bpmn2ModelerFactory.getInstance 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522329">Bug 522329</a> Correct maven warnings in BPMN2-Modeler build (
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522448">Bug 522448</a> NullPointerException below DefaultPasteBPMNElementFeature.setId
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522589">Bug 522589</a> NullPointerException in ReconnectBaseElementFeature.preReconnec
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522669">Bug 522669</a> NullPointerException in JbpmModelUtil.getElementParameters (
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522764">Bug 522764</a> SWTException below DesignEditor.updateTabs
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=522768">Bug 522768</a> NullPointerException in ListAndDetailCompositeBase.addDomainListener
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=525237">Bug 525237</a> ArrayIndexOutOfBoundsException below ManhattanConnectionRouter.calculateRoute
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=525246">Bug 525246</a> NullPointerException in AnchorUtil.getAnchorContainer
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=525282">Bug 525282</a> IndexOutOfBoundsException below GatewayDetailComposite$SequenceFlowsListComposite$1.selectionChanged
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=525285">Bug 525285</a> NullPointerException in StyleUtil.findDiagram 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=525779">Bug 525779</a> NullPointerException in DIUtils.createDiagram
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=525835">Bug 525835</a> SWTException below ListAndDetailCompositeBase.setVisible (thrown in Widget.checkWidget)
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=497647">Bug 497647</a> Cannot set value of Grid Width and Height to 0 through preferences page 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=497744">Bug 497744</a> Disallowed characters are not shown correctly
		</li>
	</ul>
			
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
