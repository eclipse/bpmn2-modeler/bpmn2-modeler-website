<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.1.1 - Service Release (January 9, 2015)</h4>
	<p>
	This release includes several bug fixes from the previous 1.1.0 release.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=435763">Bug 435763</a> - IReconnectionFeature don't work with custom links.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417654">Bug 417654</a> - Element selection should not be reset when switching back from source.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=439149">Bug 439149</a> - Tab switching between Process and SubProcess diagrams
    causes incorrect diagram to be drawn.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=438604">Bug 438604</a> - Offset in x,y coordinates
    between *.bpmn and exported Image.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=439929">Bug 439929</a> - Problems with importing WSDL that contains embedded XSD
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423381">Bug 423381</a> - Manhattan Router needs work.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425689">Bug 425689</a> - Multiple outgoing sequence flows of a task.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=440827">Bug 440827</a> - Unable to remove participant process (whitebox)
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=443140">Bug 443140</a> - Source for catch Link Events should be automatically
    configured
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1113139">RH Bug 1113139</a> - Special
    characters are not allowed in the Task GroupID
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=443173">Bug 443173</a> - In Condition Expression editor, "Condition Language" field
    doesn't show previously set value
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=442789">Bug 442789</a> - Unable to set Message for Rceive Task
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=442752">Bug 442752</a> - Connecting two elements from Palette with Sequence flow
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=441057">Bug 441057</a> - Exception while creating connection form node to another
    connection
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1144961">RH Bug 1144961</a> - Remove unnecessary Formal Expression features from details composite for
    "Actor" expressions.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=432046">Bug 432046</a> - Unclear how to set AdHocSubProcess completion condition.
    Hide Sub Process property tab for AdHoc Sub Process.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=445017">Bug 445017</a> - Regression: Tool Profile selection no longer works
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=444373">Bug 444373</a> - Missing attribute StandardLoopCharacteristics in
    Preferences
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=445139">Bug 445139</a> - jBPM Simulation Parameters are not saved
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1139130">RH Bug 1139130</a> - Signal "ID" should
    be renamed in the eclipse BPMN designer in the same way as web designer
		</li>


		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=445230">Bug 445230</a> - Appearance preferences not persisted between sessions
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=446154">Bug 446154</a> - Unable to set onEntry/onExit script for embedded AdHocSubprocess
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417015">Bug 417015</a> - Missing 'drools' expression language for conditions.
		</li>

		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1150074">RH Bug 1150074</a> - Add logic to properly display Start Events "Is Interrupting" and Boundary Events "Cancel Activity" attributes.
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1150060">RH Bug 1150060</a> - Don't use targetNamespace prefixes for BPMN2 object ID references
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1149515">RH Bug 1149515</a> - Bpmn2 Diagram
    Editor which comes with JBDS Integration Stack does not provide facility
    of writing On Entry / On Exit scripts for custom service tasks
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1146739">RH Bug 1146739</a> - save progress is stopped
		</li>

		<li>
			<a href="https://issues.jboss.org/browse/JBTIS-332">JBTIS Bug 332</a> - update poms to generate source bundles
		</li>




		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=436262">Bug 436262</a> - DefaultMarkerBehavior is not cooperative. Causes files with
    many errors to appear to "hang" during loading.
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=448073">Bug 448073</a> - Unable add more imports to *.bpmn2 model
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=447659">Bug 447659</a> - Unable to delete Input Data Mapping of User Task
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=448947">Bug 448947</a> - Modeler generates wrong xml after adding local process variable
		</li>




		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1144961">RH Bug 1144961</a> - need a custom
    detail composite for Formal Expressions.
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1155707">RH Bug 1155707</a> - changed name of
    DataOutput for CatchEvents, and DataInput for ThrowEvents to "event".
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=448573">Bug 448573</a> - Error during adding element 'Lane' from category
    'Swimlanes'.
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=446156">Bug 446156</a> - Non jbpm options present in the editor when jbpm runtime is
    selected
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=443072">Bug 443072</a> - Strange behavior of 'Append *' context menu
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=449371">Bug 449371</a> - Support for replication of Pools and Messageflows in
    Process hierarchy.
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1168846">RH Bug 1168846</a> - change "Called Activity" label to "Called Element".
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=444257">Bug 444257</a> - Validation of 'Data Object' connected to 'Script Task'
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=450826">Bug 450826</a> - Wrong validation of Event Sub-Process.
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1172177">RH Bug 1172177</a> - Business Rule Task
    should support I/O Parameter mapping
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=454749">Bug 454749</a> - Wrong Target Runtime is selected if a project does not
    define one
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1173140">RH Bug 1173140</a> - Process id is not
    modifiable (for jBPM plugin only)
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/SWITCHYARD-2484">RH Bug SWITCHYARD-2484</a> - SY Service Task
    generates wrong data input references
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1147940">RH Bug 1147940</a> - Incorrect
    validation of Ad Hoc process.
		</li>

		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=427470">Bug 427470</a> - Refactor validation classes to allow Target Runtimes to
    override specific BPMN 2.0 core constraints.
    https://bugzilla.redhat.com/show_bug.cgi?id=1147940 - Incorrect
    validation of Ad Hoc process
		</li>

	</ul>
	<p>
	This release has also added some new features and fixes:
	</p>
	<ul>
		<li>
			Handling of Pools and nested Lanes has been fixed. In some cases, deleting a nested Lane caused an NPE.
		</li>
		<li>
			ServiceTask, SendTask and ReceiveTask I/O mapping and refresh problems were fixed.
		</li>
		<li>
			The BPMN 2.0 <BPMNLabelStyle> DI element is now supported and referenced in <BPMNLabel> elements.
		</li>
		<li>
			Multiple connections origination or terminating on the same edge of a rectangle shape (Activity, Sub Process, etc.) are now
			evenly spaced along that edge for clarity.
		</li>
		<li>
			The Graphiti version dependency has been narrowed to 0.11.0 instead of allowing 0.10.0 - 0.12.0 because a Graphi API method was changed.
		</li>
		
		<li>
			The model validation framework has been reworked a bit to allow extension plug-ins to override model constraints for a particular BPMN2 model type.
			See <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=427470">Bug 427470</a> for more details.
		</li>

	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
