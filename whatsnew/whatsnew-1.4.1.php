<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2017
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.4.1 Eclipse Oxygen Release (September 20, 2017)</h4>
	<p>
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=500899">Bug 500899</a> NPE dropping element from palette in empty diagram (
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=518980">Bug 518980</a>  On Entry and On Exit Scripts not shown for User Tasks 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=519325">Bug 519325</a> BPMN2-Modeler exception thrown - caught by AERI
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=513903">Bug 513903</a> NPE when opening preference pages and no default tool profile set 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=517257">Bug 517257</a> Eclipse Oxygen: Property Dialog - content not visible after open
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=512240">Bug 512240</a> Introduce unit tests for BPMN2 Modeler 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=506624">Bug 506624</a> The property 'text.limit' is not applied to all text fields
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=521120">Bug 521120</a> IllegalStateException below Bpmn2Preferences.keyExists
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=520662">Bug 520662</a> Very long vertical sub-processes are cropped in the editor 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=521393">Bug 521393</a> NullPointerException in ModelHandler.createLane
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=521399">Bug 521399</a> NPE in DefaultBPMN2EditorDiagramBehavior.selectPictogramElements
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=521419">Bug 521419</a> NPE in DIUtils.getDiagram
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=521428">Bug 521428</a> NPE in MoveDataFeature.postMoveShape
		</li>
	</ul>
			
	<p>
	This release also includes an enhancement that has been requested several times in the past, namely the ability to define custom Work Item editing dialogs.
	Please refer to the <a href="https://wiki.eclipse.org/BPMN2-Modeler/JBoss-Custom-WID-Editor">wiki page</a> that describes this new feature.
	</p>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
