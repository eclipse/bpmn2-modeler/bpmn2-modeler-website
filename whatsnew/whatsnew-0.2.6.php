<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 0.2.6 Bug Fixes and New Features (June 7, 2013)</h4>
	What's new in this version:
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=406794">Bug 406794</a> - UserTask generated code missing outputSet in ioSpecification.
			<p>
			The ioSpecification element requires at least one outputSet, even if no output mappings are defined.
			</p>
		</li>
		<li>
			<a href=""https://bugs.eclipse.org/bugs/show_bug.cgi?id=409839>Bug 409839</a> - Lane coordinates & size not initialized after creation.
			<p>
			After creating a Lane the coordinates and size are all zero. These values are updated properly only after the Lane is moved or resized.
			</p>
		</li>
		<li>
			New "On Entry" and "On Exit" script sections were added to the Service Task property sheet for the jBPM Target Runtime. 
		</li>
		<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=409962">Bug 409962</a> - Missing 'y' attribute in waypoints when creating a self reference.
				<p>
				Creating a new shape or moving a shape such that its top or left edge is outside the diagram, would corrupt the file.  
				</p>
		</li>
		<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=408559">Bug 408559</a> - Current selection changed by moving selected activity or saving changes.
				<p>
				This was caused by graphic element selection conflicts with the Outline viewer.
				</p>
		</li>
		<li>
			Image loading for Custom Tasks was broken by the migration to Graphiti 0.10.0. This has been fixed.
		</li>
		<li>
			The Multi-tab functionality was broken by the migration to Graphiti 0.10.0. This prevented the creation of new diagrams (e.g from the "Diagram" main menu, or the "whitebox" context menu action)
			and sub-diagrams ("push down" context menu action.) 
		</li>
		<li>
			Work Item Definition (WID) configuration for jBPM has been improved. The custom task parameters can now be edited from the I/O Parameters property tab instead of a popup dialog.
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
