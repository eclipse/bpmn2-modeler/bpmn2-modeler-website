<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.1.6 - Service Release (March 30, 2016)</h4>
	<p>
	This release contains some more back-ports of some critical bug fixes made in later (Mars and Neon) versions.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1308969">RH Bug 1308969</a> - Manual Task - missing data type for "task name" and "actor id"
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1316040">RH Bug 1316040</a> - Default service task I/O parameters not shown in "I/O Parameters" tab
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1317432">RH Bug 1317432</a> - Setting message of receive task throws exception
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489441">Bug 489441</a> - Multiline text fields behaving badly
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489266">Bug 489266</a> - Unable to add text annotation association
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489265">Bug 489265</a> - Unable to add data object association
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=490300">Bug 490300</a> - NPE thrown when WID files contain duplicate entries
		</li>

	</ul>
	<p>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
