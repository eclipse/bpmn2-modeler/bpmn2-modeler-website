<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 0.2.5 (Still More) Bug Fixes and New Features (April 25, 2013)</h4>
	What's new in this version:
	<ul>
		<li>
			A file import feature has been added that allows files without DI (Diagram Interchange) metadata to be opened and edited.
			This is also a first-cut for an automatic layout feature that is in the works for a future version.
			Unfortunately this introduced some regressions, which also had to be dealt with before this release was published.   
		</li>
		<li>
			The jBPM Target Runtime extension model has been updated to reflect the new BPSim version 1.0 model definition.
			A new User Preference page named "jBPM Runtime" has been added which contains a checkbox to enable/disable the serialization of BP Simulation parameter elements.
			This also affects display of the "Simulation" property tab in the editor.
		</li>
		<li>
			All BPMN2 Modeler plugins and dependencies (including the MDT BPMN2 metamodel project plugins) are now signed. 
		</li>
		<li>
			This will be the last officially supported version for eclipse 3.6.2.
			Helios is now almost 3 years old and it takes an inordinate amount of work to keep the editor code in sync.
			This is not the eclipse you are looking for...move along <img src="/bpmn2-modeler/images/wink.gif" width="20" height="20" border="0"/>
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
