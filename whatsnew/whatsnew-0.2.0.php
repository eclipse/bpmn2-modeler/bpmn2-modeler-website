<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 0.2.0 API Stabilization release (December 12, 2012)</h4>
	Here are the highlights of what's new in this version:
	<ul>
		<li>
			The "customTask" extension can now be used to extend any BPMN2 element, not just Tasks. This allows you, for example to define custom Text Annotations in your extension plugin to specialize these elements for analysis tools or runtime engines.
		</li>
		<li>
			The "propertyTab" extension was simplified to allow the BPMN2 element attributes to be specified in the plugin.xml instead of having to write java code.
		</li>
		<li>
			The Runtime Extension interface was enhanced to allow specification of supported type and expression language.
		</li>
		<li>
			An object creation hook was added to the Runtime Extension interface to allow the extension plugin to more easily extend BPMN2 elements.
		</li>
		<li>
			The Example Runtime and JBPM5 extension plugins have been split out as separate installable features.
		</li>
		<li>
			The Example Runtime plugin has been greatly enhanced to illustrate many more of the new API features.
		</li>
		<li>
			A lot of the Property Sheets have been reworked for better usability.
		</li>
		<li>
			A New Process Wizard was added to the JBPM5 plugin, separate from the "generic" BPMN2 wizard. This creates JBPM5-specific process files and initializes the project settings as necessary.
		</li>
		<li>
			Many new bug fixes.
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
