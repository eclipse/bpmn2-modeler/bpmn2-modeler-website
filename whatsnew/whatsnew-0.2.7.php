<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 0.2.7 Final Incubation Release (August 30, 2013)</h4>
	The BPMN2 Modeler project has been in development for over a year now and this will be the last version during the incubation stage before what we hope will be a 1.0 graduation release.
	We are planing on a "final push" with some help from the user community, to find and stomp out the last few remaining bugs in the next month or so.
	See the announcement on <a href="http://zarroboogsfound.com">Zarroboogsfound.com</a> for details. Thanks to everyone in the eclipse user community for your help!
	<p></p>
	<p>
	Here is a list of the bugs that have been addressed with this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=370209">Bug 370209</a> - Validate that participants associated with message flow are consistent with those on the associated choreography task
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=374000">Bug 374000</a> - Need automatic layout of pools and lanes - cleaned up connection routing after pool/lane orientation change.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=379504">Bug 379504</a> - Data flow enhancements
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=383962">Bug 383962</a> - Reconcile BPMN2 element creation mechanisms. Started moving BPMN2 element factory to Bpmn2ModelerFactory.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=406794">Bug 406794</a> - jbpm UserTask missing "outputSet" in "ioSpecification"
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=408559">Bug 408559</a> - Reworked the outline viewer so it behaves a little better.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=408561">Bug 408561</a> - Empty actor script language can only be selected at the very beginning
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=408561">Bug 408561</a> - Add null/empty selection in Script Language combo
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=408564">Bug 408564</a> - Remove null/empty selection for tool palette profile
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=409698">Bug 409698</a> - Send to front when selecting shape.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=409839">Bug 409839</a> - Lane coordinates & size not initialized.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=409959">Bug 409959</a> - When an activity is dragged outside of a ad-hoc-subproces then it will still remain a part of it.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=409962">Bug 409962</a> - Force serialization of DC:Point even if x,y are 0.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=410104">Bug 410104</a> - Problems with properties dialog and cancel.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=410267">Bug 410267</a> - Unable to delete output parameter. Applied patch from Andrey Pavlenko.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=410270">Bug 410270</a> - Unable to import files with .bpmn2 extension. Applied patch from Andrey Pavlenko.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=410274">Bug 410274</a> - Changing Output Parameter Mapping is not reflected in the table. Applied patch from Andrey Pavlenko.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=411111">Bug 411111</a> - Add zoom function to mouse wheel
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=411174">Bug 411174</a> - Can't add or remove I/O params on custom work item definitions.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=411415">Bug 411415</a> - Undo after moving an Activity inside a SubProcess causes editor crash. Regression caused by fix for Bug 409698.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=411421">Bug 411421</a> - Event Definition decorator appears inside Event Label.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=411511">Bug 411511</a> - DataInputAssociation assignment element is incomplete after removing the "from" expression. Don't save Assignment elements if they are invalid.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=411541">Bug 411541</a> - File save takes an unusually long time when jBPM User Task property tab is visible. Refactored some code to cache tooltip text lookups, which was enough to speed up the resource change notification loop.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=411648">Bug 411648</a> - Duplicate object IDs are created under certain conditions.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=411717">Bug 411717</a> - Feature serialization for some elements does not match XSD used by jBPM parser.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=411796">Bug 411796</a> - jBPM XML Handler throws NPE searching for forward references in Multi-instance SubProcess
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=412020">Bug 412020</a> - Extra tags for xsi:schema location and xsi:namespace declaration
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=412025">Bug 412025</a> - New Message Object is created even though cancel was hit.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=412027">Bug 412027</a> - Operation name counter increasing despite cancel was hit.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=412065">Bug 412065</a> - Import interface to process imports also constructors.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=412066">Bug 412066</a> - Unable to remove imported interface.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=412156">Bug 412156</a> - WSDL Import generates incorrect XML references for newly created Interfaces and Operations.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=412164">Bug 412164</a> - "Failed to create the part's controls" error when opening bpmn file.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=412204">Bug 412204</a> - Call Activity - Process and Global Task wrapping problems.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=412548">Bug 412548</a> - Compensation boundary event visualization is incorrectly generated. Enhanced ShapeLayoutManager to correctly deal with placement of Boundary Events.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=413070">Bug 413070</a> - Clean up and expand model validation.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=413074">Bug 413074</a> - Boundary Events can be moved to a different Activity but remain attached to original Activity. We should not allow Boundary Events to be moved to another Activity in the first place.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=413118">Bug 413118</a> - Deleting a graphical element does not refresh Outline Viewer.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=413210">Bug 413210</a> - BPMN2Modeler - Group element does not appear over more than one Lane.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=413563">Bug 413563</a> - Flow nodes disappearing when opening a model containing Data Objects.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=414163">Bug 414163</a> - definitionalCollaboration reference not set
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=414207">Bug 414207</a> - DND of a Message onto a MessageFlow should associate that message with the MessageFlow. Added clipping strategy for connections.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=414442">Bug 414442</a> - CustomTask specified not activated when added via button bar.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=415003">Bug 415003</a> - Impossible to reroute data association endpoint on data object inside of a Lane
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=415158">Bug 415158</a> - Property Tab displays wrong label for Data Object itemSubjectRef
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=415186">Bug 415186</a> - Custom Tasks only added to ToolPalette for Process Diagrams
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=415190">Bug 415190</a> - Regression: Call Activity's Called Element Ref is causing resource loading failures
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=415255">Bug 415255</a> - Keep absolute position of inner element when resizing container using left or top edge
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=415724">Bug 415724</a> - Fix handling of ESC key for popupMenu actions
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416076">Bug 416076</a> - In some cases it is not possible to reconnect a connection to a different anchor point on an Activity.
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=970577">Red Hat Bug 970577</a> - Added Cancel and Terminate event definitions to jbpm model enablements
		</li>
	</ul>
	<p>
	This release has also added quite a few enhancements and new features:
	</p>
	<ul>
		<li>
			Copy/Paste has been implemented. It is now possible to select one or more Activities, Gateways, etc. and copy/paste them within the same Diagram.
			Any connections shared with the selected shapes are copied as well. This functionality does not yet work across files, but it is planned for a future release. 
		</li>
		<li>
			The behavior of the Group artifact has been greatly improved. It is now possible to "lasso" shapes on the Diagram by moving or resizing a Group;
			the shapes will then move along as part of the Group.  
		</li>
		<li>
			Model validation has been improved and cleaned up. A new Project Preference named "Perform Core BPMN 2.0 Validation" will enable or disable core BPMN 2.0 validation rules
			for Target Runtime plug-ins that provide their own model validation constraints. The extension API has been enhanced to allow Target Runtime plug-ins to define their own
			type of problem markers; this is demonstrated in the jBPM Target Runtime plug-in.
		</li>
		<li>
			A new connection type, Data Association, has been added to the toolpalette. This allows the modeling of data flows between Activities and Participants.
			Creating, or reconnecting a Data Association will also create or re-map an Activity's I/O Parameters as needed.
		</li>
		<li>
			A minor BPMN2 model issue (<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=323240">see Bug 323240</a>) has been fixed and contributed back to the
			<a href="http://www.eclipse.org/modeling/mdt/?project=bpmn2#bpmn2">BPMN2 metamodel project</a>.
			This prevented XML serialization and parsing of a Standard Loop Characteristics element. The BPMN2 Modeler now uses the
			<a href="http://download.eclipse.org/modeling/mdt/bpmn2/updates/milestones/S20130822">latest milestone release</a> of the metamodel project.
		</li>
		<li>
			The Business Model Outline Tree Viewer has been enhanced to include new BPMN2 model elements, and organized to show their relationships in a way that makes more sense.
			For example: Event Definitions are shown as child nodes of Boundary Events and Boundary Events are child nodes of their Activities;
			Interfaces, Operations and Messages are shown as parent/child nodes in the Outline tree view, etc.
		</li>
		<li>
			The Custom Task example plug-in has been enhanced to demonstrate how to extend Boundary Events and Event Definitions.
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
