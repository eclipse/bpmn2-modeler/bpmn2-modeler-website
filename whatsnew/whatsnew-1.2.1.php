<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.2.1 - Service Release (November 19, 2015)</h4>
	<p>
	This release fixes some regressions introduced in the refactoring work of the previous 1.2.0 release.
	We have also added support for Graphiti versions 0.12 and 0.13 for compatibility with Mars service releases.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1189454">RH Bug 1189454</a> - Multiple-Instance Loop Characteristics and process variables are not linked.
			Added a Java typespec field editor to support parameterized Java types.
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1213445">RH Bug 1213445</a> - User can't specify Interface for Service Task.
			Added ability to edit Interface name and implementation from ServiceTask, ReceiveTask and SendTask Operation editor.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=467747">Bug 467747</a> - ObjectPropertyProvider does not cleanup it's resource
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=471357">Bug 471357</a> - target runtime refactoring
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/BPMSPL-124">RH Bug BPMSPL-124</a> - Task metadata being improperly stored in I18NText database table.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=472174">Bug 472174</a> - Event SubProcess may not have incoming or outgoing sequence flows
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1253594">RH Bug 1253594</a> - Diagram Editor does not allow to use a variable for "called element" in CallActivity
		<li>
			<a href="https://issues.jboss.org/browse/JBPM-4753">RH Bug JBPM-4753</a> - Selection change handler in DesignEditor class should be filtering out events that do not apply to the current editor instance.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=477003">Bug 477003</a> - UI should use EMF Enumerator class' getName() instead of literal value for string externalization.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=476672">Bug 476672</a> - jBPM Process ID validation is incorrect
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1235689">RH Bug 1235689</a> - Custom Service Task is losing information after round-tripping from Web Designer to JBDS
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=477103">Bug 477103</a> - Incorrect use of "targetNamespace" to identify the Target Runtime of a bpmn2 file
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1263526">RH Bug 1263526</a> - Conditional boundary event can't be edited.
			Caused by a regression during TargetRuntime refactoring.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=479972">Bug 479972</a> - fix palette viewer labels for translations
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=480775">Bug 480775</a> - DND of an Activity onto a Connection does not position the new Activity correctly
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=479851">Bug 479851</a> - Context menu Documentation Dialog does not store correct Documentation object
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1227392">RH Bug 1227392</a> - Second level sub-process connections not rendering correctly
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1265413">RH Bug 1265413</a> -  Multi Instance subprocess not valid when validated
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1263551">RH Bug 1263551</a> - Missing General tab in 'Edit Lane' popup.
			This always forces the display of a Tab in the popup dialog, even if there is only one Tab.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=480881">Bug 480881</a> -  Morph element that involves a compound feature causes ClassCastException
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1211647">RH Bug 1211647</a> -  Definition of 'Data Type' in 'Create New Error' dialog fails
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=465939">Bug 465939</a> - Call activity loop cause StackOverflowException
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=464052">Bug 464052</a> - Adding a 3rd Pool element is not working correctly when Full-Profile is active
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1218267">RH Bug 1218267</a> - Using escalation discards data type of process variable
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=481761">Bug 481761</a> - Regression: Message selection for MessageFlow should be a combo box
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1249658">RH Bug 1249658</a> - Input parameters of throw event affect process variables
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1232661">RH Bug 1232661</a> - Property "independent" is set to 'false' when CallActivity is created by jBDS
		</li>
	</ul>
	<p>
	This release has also included the following enhancements:  
	</p>
	<ul>
		<li>
			Target Runtime code refactoring (see <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=471357">Bug 471357</a> and
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=383962">Bug 383962</a>).
			The goal of this was to remove static variables which caused problems in some cases when identifying the correct Target Runtime.
		</li>
		<li>
			Added support for selectively serializing FormalExpression.body and Documentation.text as CDATA instead of attribute values.
		</li>
		<li>
			String externalization and cleanup (see <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=474434">Bug 474434</a>).
		</li>
		<li>
			Added support for Graphiti 0.13.0.
		</li>
		<li>
			The conversions of Process and Collaboration Diagrams has been fixed.
			When a diagram contains only a single Process and a new Pool (a.k.a. "Participant") is added,
			the diagram is converted to a Collaboration Diagram containing the new Participant and a "default" Participant that references the original Process.
			<p/>
			Conversely, if a Collaboration contains only two Participants and one is removed,
			the diagram is converted to a Process Diagram containing the Process of the remaining Participant.
			See <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=465939">Bug 465939</a>.
		</li>
	</ul>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
