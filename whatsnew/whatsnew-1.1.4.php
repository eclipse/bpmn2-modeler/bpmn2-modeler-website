<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.1.4 - Service Release (December 4, 2015)</h4>
	<p>
	This release includes several bug fixes from the previous 1.1.3 release.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=462970">Bug 462970</a> - SubProcess added via menu displayed on elements absorbs inner elements.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=463362">Bug 463362</a> - "Data Output has no Data Type" on any signal boundary event.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=469872">Bug 469872</a> - Custom SequenceFlow's extended properties doesn't work.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=464028">Bug 464028</a> - Missing "waitForCompletion" attribute for "Intermediate Throw Compensation Event".
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=470108">Bug 470108</a> - Multiline text fields are persisted with "\r\n" as line delimiters on Windows OS.
		</li>
			
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=470295">Bug 470295</a> - Select All + Delete causes ClassCastException if selection contains a Group.
		</li>
			
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=472174">Bug 472174</a> - Event SubProcess may not have incoming or outgoing sequence flows.
		</li>
			
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=476672">Bug 476672</a> - jBPM Process ID validation is incorrect.
		</li>
			
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=477103">Bug 477103</a> - Incorrect use of "targetNamespace" to identify the Target Runtime of a bpmn2 file.
		</li>
			
			
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1182875">RH Bug 1182875</a> - Package Name attribute should be validated by BPMN 2 diagram editor before running the process.
		</li>
			
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1200000">RH Bug 1200000</a> - Wrong validation of process with parallel gateway.
		</li>
			
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1189454">RH Bug 1189454</a> - Multiple-Instance Loop Characteristics and process variables are not linked.
		</li>
			
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1213445">RH Bug 1213445</a> - User can't specify Interface for Service Task.
    		Added ability to edit Interface name and implementation from ServiceTask, ReceiveTask and SendTask Operation editor.
		</li>
			
		<li>
			<a href="https://issues.jboss.org/browse/BPMSPL-124">RH Bug BPMSPL-124</a> - Task metadata being improperly stored in I18NText database table.  This adds a Description field to UserTask.
		</li>
			
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1253594">RH Bug 1253594</a> - Diagram Editor does not allow to use a variable for "called element" in CallActivity.
		</li>
			
		<li>
			<a href="https://issues.jboss.org/browse/JBPM-4753">RH Bug JBPM-4753</a> - Selection change handler in DesignEditor class should be filtering out events that do not apply to the current editor instance.
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1235689">RH Bug 1235689</a> - Custom Service Task is losing information after round-tripping from Web Designer to JBDS
		</li>
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=479851">Bug 479851</a> - Kontextmenu Documentation Dialog does not store correct Documentation object
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1227392">RH Bug 1227392</a> - Second level sub-process connections not rendering correctly
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1265413">RH Bug 1265413</a> -  Multi Instance subprocess not valid when validated
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1263551">RH Bug 1263551</a> - Missing General tab in 'Edit Lane' popup.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=480775">Bug 480775</a> - DND of an Activity onto a Connection does not position the new Activity correctly
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=480881">Bug 480881</a> -  Morph element that involves a compound feature causes ClassCastException
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=481005">Bug 481005</a> - Morph to UserTask in jBPM plugin is missing ioSpecification and DataInputs
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1211647">RH Bug 1211647</a> -  Definition of 'Data Type' in 'Create New Error' dialog fails
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=465939">Bug 465939</a> - Call activity loop cause StackOverflowException
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=464052">Bug 464052</a> - Adding a 3rd Pool element is not working correctly when Full-Profile is active
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=481500">Bug 481500</a> - Class Cast Exception caused by MultiUpdateFeature
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1218267">RH Bug 1218267</a> - Using escalation discards data type of process variable
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=481761">Bug 481761</a> - Regression: Message selection for MessageFlow should be a combo box
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1249658">RH Bug 1249658</a> - Input parameters of throw event affect process variables
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1232661">RH Bug 1232661</a> - Property "independent" is set to 'false' when CallActivity is created
		</li>
	</ul>
	<p>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
