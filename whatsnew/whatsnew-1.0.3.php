<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.0.3 - Service Release (October 10, 2014)</h4>
	<p>
	This release includes several bug fixes from the previous 1.1.0 release.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=431496">Bug 431496</a> - ServiceTask: user added implementation is not durable.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=432341">Bug 432341</a> - Timer event definition - duration and cycle can be set at the same time
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=430955">Bug 430955</a> - Unable to drag incoming connection to intermediate catch event
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=431570">Bug 431570</a> - Unable to edit operation
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=432046">Bug 432046</a> - Unclear how to set AdHocSubProcess completion condition
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=433437">Bug 433437</a> - No option for Java implementation of service task
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=444913">Bug 444913</a> - Missing Triggered By Event on sub process
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=432782">Bug 432782</a> - Multi Instance Activity variable id set incorrectly via JBDS BPMN2 Editor
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=430953">Bug 430953</a> - boundaryEvent generates dataOutput/dataOutputAssociation/outputSet even when none is required
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=431499">Bug 431499</a> - Empty window shown when an activity is double clicked
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=433100">Bug 433100</a> - Inconsistent section naming in process 'data items' tab
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=433704">Bug 433704</a> - Differences to jBPM Web Designer and BPMN2 Modeler
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=442789">Bug 442789</a> - Unable to set Message for Receive Task
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=443786">Bug 443786</a> - Setting priority of outgoing connections in diverging exclusive gateway
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=442752">Bug 442752</a> - Connecting two elements from Palette with Sequence flow
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1113139">RH Bug 1113139</a> - Special characters are not allowed in the Task GroupID
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1090792">RH Bug 1090792</a> - multi-instance loop subprocess can not define the ID in BPMN Diagram Editor
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1090843">RH Bug 1090843</a> - Error boundary event on service task has wrong attachedToRef identifier
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1144961">RH Bug 1144961</a> - "Script language" for Actors and I/O Parameters
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1090855">RH Bug 1090855</a> - Cannot add additional input parameters to a service task
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1068075">RH Bug 1068075</a> - Process definition gets corrupted without any actual changes
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1139130">RH Bug 1139130</a> - Signal "ID" should be renamed in the eclipse BPMN designer in the same way as web designer
		</li>
	</ul>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
