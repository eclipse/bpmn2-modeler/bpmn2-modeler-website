<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.0M1 - Milestone for Project Graduation and 1.0 Release (October 15, 2013)</h4>
	<p>
	This is the first, and only planned, milestone release before what we hope will be a successful graduation review and 1.0 release.
	Thanks to everyone who participated in the <a href="http://zarroboogsfound.com">Bug Hunt Contest</a>.
	</p>
	<p>
	This release, as well as the 1.0 follow-on, will only be provided for Graphiti 0.10.x. That means Eclipse Kepler and beyond - Juno and previous versions of Eclipse, which use an earlier version of Graphiti, will no longer be supported.
	This does not mean that the BPMN2 Modeler will not run on Juno and prior versions of Eclipse, it simply means that you must upgrade Graphiti from 0.9.x to 0.10.x or later in your Juno workbench. 
	</p>
	<p>
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416481">Bug 416481</a> - Revert Documentation property entry back to multiline one 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416719">Bug 416719</a> - NPE on message type browse. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416726">Bug 416726</a> - Missing cancel event definition on End Event. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416727">Bug 416727</a> - Start event missing event types in properties. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416750">Bug 416750</a> - NPE in BooleanObjectEditor 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416769">Bug 416769</a> - sequence flow extension not permitted 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416785">Bug 416785</a> - Eclipse BPMN2 Modeler DataType Mappings are cumbersome 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416795">Bug 416795</a> - Cannot add a new operation to interface. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417002">Bug 417002</a> - Empty script language should not produce validation error for jBPM 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417014">Bug 417014</a> - Error when trying to create a process in a java package. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417015">Bug 417015</a> - Missing 'drools' expression language for conditions. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417088">Bug 417088</a> - Manhattan style for router should be the default in style 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417181">Bug 417181</a> - Rendering of Sequence Flow Condition Expression causes an SWT "No more handles" error 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417197">Bug 417197</a> - Using window "X" close button instead of Cancel in property dialog does not roll back transaction 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417203">Bug 417203</a> - Cannot save process definition which contains a local variable. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417207">Bug 417207</a> - Error Event should not have Cancel Activity 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417320">Bug 417320</a> - outputSet not generated for custom task 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417345">Bug 417345</a> - Unable to set condition on Gateway. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417350">Bug 417350</a> - New variable data type not getting selected. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417355">Bug 417355</a> - Able to add a new "Data Type" with no name. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417357">Bug 417357</a> - Unify labels for new types. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417361">Bug 417361</a> - Cannot add anything to canvas when process was created as a BPMN2 model. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417364">Bug 417364</a> - Append activity in subprocess places the activity out of bounds. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417513">Bug 417513</a> - Cannot change the Target Namespace in the create new Workflow dialog box 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417535">Bug 417535</a> - Quick pick combobox for the variable type does not contain the "not set" value. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417540">Bug 417540</a> - Variable table shows only 3 rows. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417615">Bug 417615</a> - NPE when creating SwitchYard project because of uninitialized BPMN2 Project Preferences 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417621">Bug 417621</a> - Loading a model causes "ClassCastException": FormData to GridData 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417623">Bug 417623</a> - The modeler seems to be unable to resolve relative file references 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417693">Bug 417693</a> - Cardinalities of inputSets and outputSets must be restricted 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417854">Bug 417854</a> - Cannot save process after an interface has been imported. 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418030">Bug 418030</a> - (Copy-)paste results in NPE 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418108">Bug 418108</a> - Process is not valid after adding user task 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418342">Bug 418342</a> - Signal Type is not visible in the Editor 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418370">Bug 418370</a> - Usability issue with the UI for adding a constraint 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418381">Bug 418381</a> - Cannot deploy simple process - No messages found 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418387">Bug 418387</a> - event definition extension does'nt work 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418544">Bug 418544</a> - IllegalArgumentException while opening some JBPM examples diagrams 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418588">Bug 418588</a> - Undo stack is corrupted after file save 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418597">Bug 418597</a> - jBPM GlobalType "identifier" attribute must be synchronized with BaseElement "id" attribute 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418696">Bug 418696</a> - Editor creates two temp files every time a file is opened and leaves one behind 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418702">Bug 418702</a> - Fix duplicate ID handling 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418930">Bug 418930</a> - jBPM New File wizard should use ".bpmn2" for file extension instead of ".bpmn" 
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=419406">Bug 419406</a> - public extension point for the tool behavior provider 
		</li>
	</ul>
	<p>
	This release has also added some new features and code samples:
	</p>
	<ul>
		<li>
			The "customTask" extension point has been enhanced so that it uses a common interface for both shapes and connections.
			The Custom Task sample plugin has been expanded to demonstrate how this is done with a Sequence Flow.
			See <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416769">Bug 416769</a> for details.
		</li>
		<li>
			Loading of empty or incomplete files has been improved.
			See <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417361">Bug 417361</a>.  
		</li>
		<li>
			Tables in Property Sheets are now automatically resized to display more information.
			See <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417540">Bug 417540</a>.
		</li>
		<li>
			Input and Output Parameter mapping has been simplified for Service Tasks, Receive Tasks, Send Tasks and Boundary Events.
			See <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417693">Bug 417693</a>.
		</li>
		<li>
			A new extension point element, "toolProvider" has been added, which allows an extension plug-in to override the
			Graphiti Tool Behavior Provider class.
			See <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=419406">Bug 419406</a>.
		</li>
		<li>
			A new sample code plug-in has been added to demonstrate how to load and navigate a bpmn file in Java.
			The code can be downloaded from the <a href="http://git.eclipse.org/c/bpmn2-modeler/org.eclipse.bpmn2-modeler.git/tree/examples/org.eclipse.bpmn2.modeler.examples.modelreader">Eclipse Git repository here</a>. 
		</li>
		
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
