<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.4.0 Eclipse Oxygen Release (June 22, 2017)</h4>
	<p>
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=501445">Bug 501445</a> Temporary workaround for Web Tools dependency issue in Oxygen release
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/RHBPMS-4330">Bug RHBPMS-4330</a> Strange 'Remove' option in the menu
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/RHBPMS-4283">Bug RHBPMS-4283</a> Context menu 'Morph Activity' is not consistent with other options
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/RHBPMS-4295">Bug RHBPMS-4295</a> Elements can not be moved from containers
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/RHBPMS-4318">Bug RHBPMS-4318</a> BPMN Process with swimlane created from business-central has been distorted while opening in JBDS
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/RHBPMS-4330">Bug RHBPMS-4330</a> Strange 'Remove' option in the menu
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=496611">Bug 496611</a> Improve validation and error reporting for non-executable Processes
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/JBPM-5636">Bug JBPM-5636</a> The Work Items in the Service Tasks palette should be ordered
		</li>
	</ul>
			
	<p>
	This release also includes an enhancement that has been requested several times in the past, namely the ability to define custom Work Item editing dialogs.
	Please refer to the <a href="https://wiki.eclipse.org/BPMN2-Modeler/JBoss-Custom-WID-Editor">wiki page</a> that describes this new feature.
	</p>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
