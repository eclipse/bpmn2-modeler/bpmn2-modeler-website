<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 0.2.3 (More) Bug Fixes and New Example(February 15, 2013)</h4>
	What's new in this version:
	<ul>
		<li>
			A new <a href="videos/CustomTask-Example-part1.htm">screencast tutorial</a> has been added to the project,
			which demonstrates how to implement the &lt;customTask&gt; extension point.
			The source code for this tutorial is available here, in the
			<a href="https://git.eclipse.org/c/bpmn2-modeler/org.eclipse.bpmn2-modeler.git/tree/examples/org.eclipse.bpmn2.modeler.examples.customtask">"examples" directory of the Git repository</a>.
		</li>
		<li>
			All extension attributes and elements must be enabled explicitly if you want them to appear in Property Sheets.
			This is done using the &lt;modelEnablement&gt; extension point. Previously any attributes or elements defined
			with the &lt;modelExtension&gt; extension point were automatically enabled.
		</li>
		<li>
			General cleanup of some unused classes in the jBPM5 Target Runtime plugin.
			The missing Task decorator icons (for, e.g. Business Rule Task, Script Task, etc.) have been restored.

		</li>
		<li>
			The handling of Messages and Message Flows attached to Participant Bands in Choreography Activities has been cleaned up.
			Previously it was possible to remove the Message Flow, leaving an unattached Message envelope shape.
			Also, a button pad (context menu) item for removing the Message has been added; this works just like pressing the "Delete" key,
			but the new button pad is complementary to its "Add Message" button.
		</li>
		<li>
			A Class Cast exception thrown when trying to create a "New jBPM Process" in a project's sub-folder has been fixed.
		</li>
		<li>
			Operation selection and creation for Service Tasks has been fixed.
		</li>
		<li>
			The Schema Selection dialog has been fixed so that it correctly finds and imports Java classes.
		</li>
		<li>
			The "Undo" menu descriptions after creating Tasks have been changed to read simply "Create Task" instead of
			displaying a verbose description of what the created Task does.
		</li>
		<li>
			A bug in the file importer that caused Pools with Process references to corrupt the file, has been fixed.
		</li>
		<li>
			Resizing a shape now causes the attached connections to be re-routed, the same way as moving the shape.
		</li>
		<li>
			The Manhattan connection router has been improved a bit so that connections do not cross over the shapes to which they are attached.
		</li>
		<li>
			The Interfaces table column label has been fixed to read "Implementation" instead of "E Class".
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
