<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>M1 milestone (August 15, 2012)</h4>
	<br>This milestone represents a major overhaul of most of the Tabbed Property Sheet framework and related code.
	The builds are available here for
		<a href="http://download.eclipse.org/bpmn2-modeler/site/M1/">Eclipse indigo (3.7)</a>
	and
		<a href="http://download.eclipse.org/bpmn2-modeler/site-helios/M1/">helios (3.6)</a>.
	<ul>
		<li>
			Code cleanup
			<br>The code that implements the "List and Detail" pattern for rendering BPMN2 element properties
			has been cleaned up so it is easier to maintain.
			Java generics were introduced to ensure that future
			runtime extension plugins will have an easier time with their implementations.
		</li>
		<li>
			Migration to Graphiti 0.9.0
			<br>The editor is now compatible with both Graphiti 0.8.2 and 0.9.0.
			There were several significant changes in Graphiti which requires us to maintain two
			separate code branches for Eclipse helios and for the latest versions of Eclipse (indigo and juno).
			<br>While we are trying very hard to keep both branches in sync, there may come a time
			when the helios version can no longer keep up with enhancements and new features as we
			continue to improve the editor.
		</li>
		<li>
			Compatibility with Eclipse juno.
			<br>A happy coincidence, I'm sure <img src="/bpmn2-modeler/images/wink.gif" width="20" height="20" border="0"/>
		</li>
		<li>
			Usability improvements
			<br>Cell editors were added to some of the lists to simplify property editing.
			<br>Configuration popup dialogs were added
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=355673">as requested here</a>.
			After dragging-n-dropping an item from the toolpalette onto the canvas, a popup dialog
			is displayed to allow configuration of certain aspects of the new item without having
			to hunt through the Tabbed Property Sheets. Of course these popups can be disabled
			through User Preference settings so we don't slow down the business analyst types who
			only want to capture a high-level flow without worrying about the details.
			<br>I/O parameter mapping for Tasks and Events has been simplified.
		</li>
		<li>
			Functionality added
			<br>"File Save As" has been added.
			<br>The editor now handles file rename/move and delete graciously, thanks to a patch contributed by the community.
			<br>A new property tab was added for Annotations, also from a community contributor
		</li>
		<li>
			Model validation
			<br>Some rudimentary model validation has been added, thanks to contributions made by the folks at Camunda.
			Future enhancements will broaden the scope to include semantic validation, the addition of error decorators
			and "Problems View" integration. 
		</li>
		<li>
			File Import enhanced
			<br>File import types now include BPMN 2.0 diagram files (i.e. other process files) as well as Java types.
			Although Java types are not explicitly called for in the BPMN 2.0 specification, in practice this is widely
			supported by execution engines and deserves first-class citizenship.
			<br>Imports automatically create the appropriate BPMN2 elements for WSDL Port Types, Operations, Message Types
			and schema types (i.e. BPMN2 Interface, Operation, Message and ItemDefinition elements).
		</li>
		<li>
			Various bug fixes, too numerous to mention.
		</li>
	</ul>
</div>

EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
