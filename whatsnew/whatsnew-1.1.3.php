<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.1.3 - Service Release (May 15, 2015)</h4>
	<p>
	This release includes several bug fixes from the previous 1.1.2 release.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=461745">Bug 461745</a> - Loading BPMN file that has no DI elements no longer works
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=461614">Bug 461614</a> - Unable to create/edit jBPM MetaData elements.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=433704">Bug 433704</a> - Differences to jBPM Web Designer and BPMN2 Modeler.
			This change adds the "Created By" field to the User Task property sheet.
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1207326">RH Bug 1207326</a> - How to set user task notification through JBDS in jBPM 6?
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1211611">RH Bug 1211611</a> - Duplicated column "Name" in list of errors
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=464589">Bug 464589</a> - CallActivity feature package not exposed
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=464699">Bug 464699</a> - BPMNEditor#getActiveEditor returns wrong editor
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=464589">Bug 464589</a> - Wrong Target Runtime is selected if a project does not define one
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=463310">Bug 463310</a> - TextObjectEditor#setMultiLine(true) not working as expected
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=463205">Bug 463205</a> - Manhattan routing is extremely inefficient and slows down editor response
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1181215">RH Bug 1181215</a> - Arrows are changed unexpectedly when Node is moved on the canvas
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=466337">Bug 466337</a> - Boundary Events are not positioned correctly when importing BPMN files without BPMNDI
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=460058">Bug 460058</a> - Badly formatted error message - 'A Custom Task with id {0} was already defined'
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=461263">Bug 461263</a> - Delete diagram context action do not delete participants
		</li>
	</ul>
	<p>
	This release has also added some new features and fixes:
	</p>
	<ul>
		<li>
			The CustomTask example has been expanded to include demonstration the use of a CDATA element.
		</li>
		<li>
			Manhattan connection routing has been greatly improved.
		</li>
	</ul>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
