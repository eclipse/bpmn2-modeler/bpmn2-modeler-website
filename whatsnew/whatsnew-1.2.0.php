<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.2.0 - Core Refactoring Release (July 8, 2015)</h4>
	<p>
	This release targets the Eclipse Mars platform and includes several bug fixes from the previous 1.1.4 release for Luna.
	The primary goal of this release is to refactor some of the Core components to reduce dependency on Eclipse IDE components.
	Here also is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=462355">Bug 462355</a> -  refactor BPMN2Editor for use outside of an IDE.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=462928">Bug 462928</a> -  Latest commit on luna (changes from Flavio) make diagram unsaveable after reopening.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=466765">Bug 466765</a> - Manual and Automatic Connection Routers cause NPE.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=466529">Bug 466529</a> - Association link starting form message flow's message figure make diagram unsaveable.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=462970">Bug 462970</a> - SubProcess added via menu displayed on elements absorbs inner elements.
		</li>
			
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1211611">RH Bug 1211611</a> - Duplicated column "Name" in list of errors
		</li>
			
	</ul>
	<p>
	This release also has some additional changes:
	</p>
	<ul>
		<li>
			The "SDK" feature, which combines both binary and source builds for the BPMN2 Modeler has been removed because it has been replaced by separated binary and source features.
		</li>
		<li>
			Updated to MDT.BPMN2 version 1.2.0
		</li>
		<li>
			Updated to Graphiti version 0.12
		</li>
	
	</ul>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
