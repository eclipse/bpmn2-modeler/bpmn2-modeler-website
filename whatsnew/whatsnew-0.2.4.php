<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 0.2.4 (Even More) Bug Fixes and New Features (April 9, 2013)</h4>
	What's new in this version:
	<ul>
		<li>
			The Java class importer now uses the eclipse JDT IType framework for better performance when dealing with large numbers of classes.
		</li>
		<li>
			A new <toolPalette> extension point has been added to allow for definition of customized Tool Palette profiles.
			Tool Palette Profiles are selectable in the Project Preferences.
		</li>
		<li>
			Enhanced the Tool Enablement Project Preferences to distinguish between BPMN2 model, and target runtime extension model elements.
		</li>
		<li>
			The Manhattan Connection Router has been improved, but is still experimental. We are working on a better algorithm.
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
