<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.3.0 - Bug Fix Release (September 14, 2016)</h4>
	<p>
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=497759">Bug 497759</a> Modeler standard types are not compatible with java standard types
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=497895">Bug 497895</a> Bug when importing models
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=497794">Bug 497794</a> Removing I/O mapping on service task removes always hidden parameters &quot;Parameter&quot; and &quot;Result&quot;
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=495291">Bug 495291</a> First input mapping of a send task is disappearing
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=497804">Bug 497804</a> User can not scroll down to &quot;On Exit Script&quot; of service task
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=497229">Bug 497229</a> Missing titles in PopUp dialogues
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=499619">Bug 499619</a> Operation editing should allow removing in/out messages
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=497758">Bug 497758</a> Modeler standard types are not compatible with java standard types
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=493708">Bug 493708</a> Documentation in WID File will be shown in Designer
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=492792">Bug 492792</a> JBDS 8.1 is not showing the custom task under diagram pallete for maven projects
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=491206">Bug 491206</a> Input parameters mapping of service task not displayed at the first time
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=500025">Bug 500025</a> Regression: Importing connections fails if BPMNEdge has no source or target element
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=482967">Bug 482967</a> Process .... has no package name
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=497543">Bug 497543</a> Adding duplicate Message to Choreography Participant causes error on save.
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
