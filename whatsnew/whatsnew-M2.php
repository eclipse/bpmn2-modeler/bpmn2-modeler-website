<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>M2 milestone (September 15, 2012)</h4>
	<br>This milestone adds some significant new features since M1.
	The builds are available here for
		<a href="http://download.eclipse.org/bpmn2-modeler/site/M2/">Eclipse indigo (3.7)</a>
	and
		<a href="http://download.eclipse.org/bpmn2-modeler/site-helios/M2/">helios (3.6)</a>.
	<ul>
		<li>
			BPMN2 Project Nature and Validation Builder
			<br>A project builder is now used to perform batch validation of the BPMN files.
			Full validation for BPMN 2.0 compliance is still not complete, but the framework for adding new
			validation logic is in place.
		</li>
		<li>
			Problem Markers
			<br>Problem View markers and figure decorators with tooltip text on the drawing canvas as well as the
			Property Sheet View have been added.
		</li>
		<li>
			Outline View
			<br>Support for an Outline View has been added. This includes tree view outlines for both the BPMN2 element structure
			(the "business logic model") and the BPMN Diagram structure (the "Digram Interchange model") as well as a thumbnail
			view of the editor drawing canvas. 
		</li>
		<li>
			Property Sheets
			<br>New Property Sheet tabs have been added to support Choreography and Collaboration diagram activities. 
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
