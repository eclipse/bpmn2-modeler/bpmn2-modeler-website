<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2018
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.5.2 Eclipse 2020-06 Release (June, 2020)</h4>
	<p>
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=550406">Bug 550406</a>  Support for BPMN Gateways with MIXED type
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=563833">Bug 563833</a>  Updated bundle-version for org.eclipse.graphiti (version 0.17.0)
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=563644">Bug 563644</a>  Updated bundle-version for the tycho plugin (version 1.1.0)
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=563298">Bug 563298</a>  Fixed ErrorDialog
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=563236">Bug 563236</a>  Fixed IPluginDescriptor from core XMLConfigElement
		</li>
		
		
	</ul>
			
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
