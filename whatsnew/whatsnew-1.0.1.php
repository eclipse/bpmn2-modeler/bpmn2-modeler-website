<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.0.1 - Service Release (December 30, 2013)</h4>
	<p>
	This release includes an online User Guide and several bug fixes encountered during the documentation process.
	</p>
	<p>
	<a href="../documentation/BPMN2ModelerUserGuide-1.0.1.pdf">User Documentation is now available online</a>. This is available as a downloadable PDF format file, but the same content is integrated in Eclipse Help and as context-sensitive help. 
	</p>
	<p>
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=422575">Bug 422575</a> - intermediate catch events validation fails
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424148">Bug 424148</a> - add name attribute for all elements in editing popup dialog
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418721">Bug 418721</a> - F2 on a selected task should allow to edit the "Name" attribute. Fix direct editing feature for all elements.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424401">Bug 424401</a> - Gateway label not visible properly
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417567">Bug 417567</a> - Unable to correctly arrange bpmn components in a process.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424471">Bug 424471</a> - Regression: Can't move ItemAwareElements
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424394">Bug 424394</a> - Bpmn2Preference throws NullPointerException
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423941">Bug 423941</a> - Element inside Pool change its position when morphing
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418598">Bug 418598</a> - Eclipse is hung-up when selecting an inner element inside a collapsed subprocess
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=416036">Bug 416036</a> - Editor requires saving the process even if it wasn't changed
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424265">Bug 424265</a> - incorect validation in ad-hoc subprocess
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424264">Bug 424264</a> - sequence flow in subprocess
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424178">Bug 424178</a> - java.lang.ClassCastException: org.eclipse.bpmn2.impl.ExpressionImpl cannot be cast to org.eclipse.bpmn2.FormalExpression
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424197">Bug 424197</a> - Ghost connectors from Sub-Process
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424246">Bug 424246</a> - Editor hangs in infinite loop when moving a shape onto a label
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=415189">Bug 415189</a> - String externalization - added 1.0.1 snapshot
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423973">Bug 423973</a> - Eclipse Luna compatibility
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418722">Bug 418722</a> - Tasks should resize when entering a longer description
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423865">Bug 423865</a> - NullPointerException removing pool and saving collaboration
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423325">Bug 423325</a> - sequence flow default name
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423752">Bug 423752</a> - Import interface to process generates an XML which cannot be parsed
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423752">Bug 423752</a> - Import interface to process generates an XML which cannot be parsed
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423426">Bug 423426</a> - Cannot add interfaces and datatype/message/error/...
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423638">Bug 423638</a> - Can't see the name of tasks dragged into expanded sub-process
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423809">Bug 423809</a> - BPMN2 diagram not displayed if location missing from import element
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=419496">Bug 419496</a> - Choice when connecting tasks in different pools
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423115">Bug 423115</a> - Need to show all Call Activity name (wrap it instead crop)
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423248">Bug 423248</a> - Enhance Custom Connection Feature to support abstract BPMN2 base class
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423094">Bug 423094</a> - ClassCastException when deleting a message flow link between two pools
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423104">Bug 423104</a> - NPE when selecting 'Advanced mapping' in 'Signal Intermediate Catch Event'
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423113">Bug 423113</a> - EClass equality check should change to isInstance(Object) call
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=422898">Bug 422898</a> - Tool Palette is empty with diagram types for which no Tool Profile is defined by the Target Runtime
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=410270">Bug 410270</a> - Unable to import files with .bpmn2 extension
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=420723">Bug 420723</a> - Support opening/saving of bpmn files from/to local file system.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=422898">Bug 422898</a> - Tool Palette is empty with diagram types for which no Tool Profile is defined by the Target Runtime
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=422895">Bug 422895</a> - Creating Group in Choreography causes stack overflow
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=422897">Bug 422897</a> - Can't open Choreography created with Savara
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=422581">Bug 422581</a> - Popup Property Dialogs ignore "Show Advanced Tab" editor behavior preference
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=422077">Bug 422077</a> - Description Tab does not get refreshed when multiple editors are open
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418167">Bug 418167</a> - Sequence flow not fully rendered. Don't clip connections if source and target are not in the same container.
		</li>
	</ul>
	<p>
	This release has also added some new features and fixes:
	</p>
	<ul>
		<li>
			The minimum required Java version was changed to 1.6 for all plugins (some had 1.7).
		</li>
		<li>
			ServiceTask, SendTask and ReceiveTask I/O mapping and refresh problems were fixed.
		</li>
		<li>
			The "Incubation" moniker was removed from all plugins.
		</li>
		<li>
			Context help was added to dialog and property pages.
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
