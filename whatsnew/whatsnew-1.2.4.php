<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.2.4 - Service Release (March 30, 2016)</h4>
	<p>
	This release fixes more regressions introduced in the refactoring work of the previous 1.2.0 release.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488792">Bug 488792</a> - Tool Palette extension point not working as expected
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488717">Bug 488717</a> - wrong layout of property tabs in property Dialog - createBindings() method
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488889">Bug 488889</a> - MultInstanceLoopCharacteristics input and output data objects are not handled properly for Tasks
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489223">Bug 489223</a> - Exception while opening bpmn Editor After Mars.2 Update
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489375">Bug 489375</a> - Regression: Create Connection feature throws NPE when called from Java code
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489266">Bug 489266</a> - Unable to add text annotation association
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489265">Bug 489265</a> - Unable to add data object association
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489442">Bug 489442</a> - Activity with Boundary Events not moved correctly when contained in a Group
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489441">Bug 489441</a> - Multiline text fields behaving badly
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=461614">Bug 461614</a> - Unable to create/edit jBPM MetaData elements.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489619">Bug 489619</a> - bindAttribute did not work in case of copy &amp; paste
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1308969">RH Bug 1308969</a> - Manual Task - missing data type for "task name" and "actor id"
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1188592">RH Bug 1188592</a> - Mapping Message from Receive Task to process variable.
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=490300">Bug 490300</a> - NPE thrown when WID files contain duplicate entries
		</li>

	</ul>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
