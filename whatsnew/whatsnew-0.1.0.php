<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 0.1.0 stable release (October 15, 2012)</h4>
	<ul>
		<li>
			Bug Fixes
		</li>
		<li>
			String externalization
			<br>Labels, prompts and error messages will be externalized to message files and will be available for translation
			in <a href="http://www.eclipse.org/babel/">Eclipse Babel</a>.
			<br><b>Pushed to a future release.</b>
		</li>
		<li>
			User Guide
			<br>A complete User Guide will be made available, either as an installable Help feature, or as an external document.
			This will be user documentation only, not API docs for editor extensibility.
			A Developer's Guide will be made available as part of a later release.
			<br><b>Pushed to a future release.</b>
		</li>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
