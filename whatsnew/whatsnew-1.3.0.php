<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.3.0 - Feature Release (June 25, 2016)</h4>
	<p>
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=383962">Bug 383962</a> Reconcile BPMN2 element creation mechanisms
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=393034">Bug 393034</a> Map Interface to ServiceTask
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=429639">Bug 429639</a> Scan classpath for .wid/.conf files to add Custom Task to the palette
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=482083">Bug 482083</a> bindAttribute - ObjectEditor is not working in some szenarios
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=482786">Bug 482786</a> Can not open models created with different runtime
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=483689">Bug 483689</a> ScriptTasks allow only 255 characters
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=484071">Bug 484071</a> Drag and drop of call activity is corrupting the sequence flow
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=486320">Bug 486320</a> Validation Error for an Property Fully qualified name
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=486970">Bug 486970</a> Only Group name should be exposed by Group Property page for jBPM
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=487503">Bug 487503</a> Switching from one editor to the other produces a SWTException
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489619">Bug 489619</a> bindAttribute did not work in case of copy&amp;pase
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=373068">Bug 373068</a> Custom task editor does not get displayed
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=393034">Bug 393034</a> Map Interface to ServiceTask
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=461614">Bug 461614</a> Unable to create/edit jBPM MetaData elements
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=477103">Bug 477103</a> Incorrect use of &quot;targetNamespace&quot; to identify the Target Runtime of a bpmn2 file
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=486051">Bug 486051</a> New EmfService.isObjectAlive()  behaviour does not play well with GAs removal
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=486289">Bug 486289</a> NPE when adding list items
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488131">Bug 488131</a> Select All + Delete causes NPE when Boundary Event is involved
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488219">Bug 488219</a> 'I/O Parameters' tab not rendered correctly
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488221">Bug 488221</a> Unable to create custom WID for palette
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488503">Bug 488503</a> ResourcePropertyTester NPE prevents proper validation
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488717">Bug 488717</a> wrong layout of property tabs in property Dialog - createBindings() method
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488792">Bug 488792</a> Tool Palette extension point not working as expected
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488889">Bug 488889</a> MultInstanceLoopCharacteristics input and output data objects are not handled properly for Tasks
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488991">Bug 488991</a> Data Items in selection lists and tables should include context information
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489223">Bug 489223</a> Exception while opening bpmn Editor After Mars.2 Update
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489265">Bug 489265</a> Unable to add data object association
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489266">Bug 489266</a> Unable to add text annotation association
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489375">Bug 489375</a> Regression: Create Connection feature throws NPE when called from Java code
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489441">Bug 489441</a> Multiline text fields behaving badly
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489442">Bug 489442</a> Activity with Boundary Events not moved correctly when contained in a Group
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489449">Bug 489449</a> Add ability to increase height of multiline text fields
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=489903">Bug 489903</a> ClassCastException for console view selection with Java Editor in focus from BPMEditor
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=490300">Bug 490300</a> NPE thrown when WID files contain duplicate entries
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=492614">Bug 492614</a> Edit button for the ID field provided by the editor
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=493058">Bug 493058</a> I/O Parameters tab in Properties view for Custom task is blank
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1141619">Bug 1141619</a> Boundary event should not be attached to Script Task in JBDS for jBPM
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1188153">Bug 1188153</a> Global Variable Details Data Type Attribute should be validated before running the process in BPMN 2 Diagram Editor
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1188592">Bug 1188592</a> Mapping Message from Receive Task to process variable
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1279289">Bug 1279289</a> Multiple &quot;None StartEvent&quot;s should be validated by BPMN2 editor in JBDS
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1288044">Bug 1288044</a> Scroll bar does not change size dynamically - making content inaccessible
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1293238">Bug 1293238</a> Not possible to add metadata entry
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1308969">Bug 1308969</a> Manual Task - missing data type for &quot;task name&quot; and &quot;actor id&quot;
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1309950">Bug 1309950</a> JBDS Bpmn2 Diagram Editor - MetaData Tag Introduces Bad Namespace
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1309953">Bug 1309953</a> JBDS Bpmn2 Diagram Editor 1.2.1.Final - Missing Prompt for Service Task Pop Up
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1316040">Bug 1316040</a> Default service task I/O parameters not shown in &quot;I/O Parameters&quot; tab
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/BPMSPL-293">JBo</a>ss Developer Login server
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/JBPM-4860">[JBPM-4860]</a> Can not save file because &quot;No targetNamespace defined&quot;
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/JBTIS-172">[JBTIS-172]</a> &quot;JBoss jbpm5 runtime extension feature&quot; is not using Red Hat license text
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/JBTIS-471">[JBTIS-471]</a> Unify component versions
		</li>
	</ul>
			
	<p>
	This release has also included the following changes and enhancements:  
	</p>
	<ul>
		<li>
			The WSIL plugin and feature have been removed from this version and will no longer be supported in future versions.
		</li>
		<li>
			The expand/collapse of SubProcesses feature has been restored. Expanding a SubProcess will now move neighboring shapes so that they are no obscured by the expanded SubProcess. 
		</li>
		<li>
			The push down/pull up features have also been restored, and support for Message Flows between Pools has been added.
		</li>
		<li>
			Property sheet redrawing has been improved: unnecessary updates caused by multiple model change notification events for the same object have been eliminated.
		</li>
	</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
