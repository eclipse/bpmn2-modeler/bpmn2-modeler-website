<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.2.3 - Service Release (February 26, 2016)</h4>
	<p>
	This release fixes more regressions introduced in the refactoring work of the previous 1.2.0 release.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1188592">RH Bug 1188592</a> - Mapping Message from Receive Task to process variable
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=485268">Bug 485268</a> - The editor directly fetches the dtd from internet without looking into the XLM catalog
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/JBTIS-471">RH Bug JBTIS-471</a> - Updated license and provider for Red Hat/JBoss
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/JBTIS-172">RH Bug JBTIS-172</a> - Renamed the jBPM5 feature to just jBPM to avoid confusion
		</li>
		<li>
			<a href="https://issues.jboss.org/browse/JBPM-4860">RH Bug JBPM-4860</a> - Can not save file because "No targetNamespace defined"
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1288044">RH Bug 1288044</a> - Scroll bar does not change size dynamically making content inaccessible
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=486289">Bug 486289</a> - NPE when adding list items
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1279289">RH Bug 1279289</a> - Multiple "None StartEvent"s should be validated by BPMN2 editor in JBDS
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1141619">RH Bug 1141619</a> - Boundary event should not be attached to Script Task in JBDS for jBPM
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=486320">Bug 486320</a> - Validation Error for an Property Fully qualified name
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=429639">Bug 429639</a> - Scan classpath for .wid/.conf files to add Custom Task to the palette
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=486970">Bug 486970</a> - Only Group name should be exposed by Group Property page for jBPM
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=484071">Bug 484071</a> - Drag and drop of call activity is corrupting the sequence flow
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=483689">Bug 483689</a> - ScriptTasks allow only 255 characters
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=487503">Bug 487503</a> - Switching from one editor to the other produces a SWTException
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1293238">RH Bug 1293238</a> - Not possible to add metadata entry
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=393034">Bug 393034</a> - Map Interface to ServiceTask
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488131">Bug 488131</a> - Select All + Delete causes NPE when Boundary Event is involved
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488219">Bug 488219</a> - 'I/O Parameters' tab not rendered correctly
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1309950">RH Bug 1309950</a> - MetaData Tag Introduces Bad Namespace
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1309953">RH Bug 1309953</a> - Improve property page redrawing by using a UIJob
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=488503">Bug 488503</a> - ResourcePropertyTester NPE prevents proper validation
		</li>

	</ul>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
