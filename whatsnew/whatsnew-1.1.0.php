<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.1.0 - Luna Simultaneous Release (June 25, 2014)</h4>
	<p>
This is a <b>big</b> release that adds several new usability enhancements, features and bug fixes.
This is also the first version that will be included in the Luna simultaneous release. 
</p>

<h4>UI and Update Site Changes</h4>
<ul>
<li>
The "Description" property tab has been renamed to "General" because that is more descriptive of what the tab actually contains.
A new "Appearance" section has been added to this property tab, which allows the colors, line styles and fonts of each graphical element
to be customized. Changing these settings generates a BPMN2 extension element <b>&lt;style&gt;</b> in the ".bpmn2" file.
</li>
<li>
A new "info" button was added to the Context Button Pad, which activates a Documentation editor.
If an element has Documentation associated with it, the text is shown in a Tool Tip when the mouse
is hovered over the "info" button. 
</li>
<li>
Labels on shapes and connections are now separate objects and are no longer clipped by the bounds of a shape.
This means that labels can be larger than their owning shapes, and the label text will be wrapped to best fit on top
of a shape.
<p/>
A User Preference setting has been added to have the editor to automatically position labels for Gateways, Events, Data Objects and Connections
either above, below, or to the left or right of the shape. If desired, the label can also be manually positioned. 
</li>
<li>
The Tool Palette has been completely reorganized to better accommodate different user roles.
Mixed diagram types are now supported, allowing you to place Pools, Choreography Activities, and Conversations all on the same diagram if desired.
</li>
<li>
Tool Palette Profiles have been simplified.
A Tool Profile no longer depends on a Diagram Type as before since there is no longer a distinction between different Diagram Types in the editor. 
</li>
<li>
Better support for Global Activities has been added to the Tool Palette.
</li>
<li>
The User Preference pages have been updated to support some of the new features:
	<ul>
	<li>
	A checkbox has been added to the main BPMN2 page, to enable or disable the generation of <b>&lt;BPMNLabel&gt;</b> elements in the ".bpmn2" file.
	</li>
	<li>
	The Editor Appearance page has been reorganized, and support for default shape sizes, label font selection and label placement has been added.
	Also, the canvas and grid line colors may now be selected.
	</li>
	<li>
	The Editor Tool Profiles page has been reorganized to support the new Tool Profile structure.
	</li>
	</ul>
</li>
<li>
Layout and resizing behavior of Pools and Lanes has been improved.
</li>
<li>
Product build dependencies have been updated to include the latest
<a href="https://www.eclipse.org/modeling/mdt/">MDT-BPMN2</a> and
<a href="https://www.eclipse.org/bpel/">SOA-BPEL Designer</a> plug-ins, which are also part of the Luna simultaneous release.
</li>
<li>
The update site now contains an installable SDK feature, which includes binaries and source code. 
</li>
</ul>

<h4>Extension Point API Changes</h4>
<ul>
<li>
The editor's model extension mechanism is now fully dynamic - it is no longer necessary to create an EMF model and generate java implementation classes.
The <b>&lt;model&gt;</b> extension element is no longer required but can be used to specify a default model namespace URI.
To use this feature, simply create a ".bpmn2config" folder in your active project and drop in your extension definition XML files.
An example of how to define one of these files is included in the Extension Point Schema documentation.
</li>
<li>
The <b>&lt;customTask&gt;</b> and <b>&lt;modelExtension&gt;</b> elements now allow you to specify a namespace URI for the model extension attributes and elements.
Also a new "decorator" attribute has been added, which references a Java class that is invoked whenever a Custom Task or Model Extension object is instantiated.
This gives the runtime plug-in an opportunity to initialize the new object or decorate with additional features.
</li>
<li>
A "label" attribute has been added to the <b>&lt;property&gt;</b> element, which allows you to specify a more descriptive text label in the editor Property Sheets and dialogs.
</li>
<li>
Setting the <b>&lt;property&gt;</b> "class" attribute to "empty" can now be used to completely hide a Property Tab.
Simply specify the ID of the tab you wish to hide in the "replaceTab" attribute. 
</li>
<li>
New Extension Point Elements have been added:
	<ul>
	<li>
	<b>&lt;dataType&gt;</b> allows you to contribute custom data types and conversion delegates for converting model extension element values to/from string type.
		An example plug-in, <b>org.eclipse.bpmn2.modeler.examples.datatypes</b> is available that demonstrates this new feature.
	</li>
	<li>
	<b>&lt;typeLanguage&gt;</b> defines a data type system supported by the Target Runtime.
		The Default Target Runtime provides support for XSD Schema and Java types only.
	</li>
	<li>
	<b>&lt;expressionLanguage&gt;</b> defines  an expression language supported by the Target Runtime.
		The Default Target Runtime provides support for XPath and Java only.
	</li>
	<li>
	<b>&lt;serviceImplementation&gt;</b> defines a Service Task implementation supported by the Target Runtime.
		The Default Target Runtime provides support for ##WebService and ##Unspecified only.
	</li>
	</ul>
</li>
<li>
The Extension Point API document has been fleshed out with examples for some of the more complex features.
</li>
</ul>

<h4>Java API Changes</h4>
<ul>
<li>
Many of the utility classes and interfaces have been refactored, and a lot of dead code has been removed.
Some of the Graphiti Feature class hierarchies have been refactored for better reuse.
</li>
<li>
Because the Expression Language and Type Language definitions have been moved to an Extension Point Element, they are no longer required in the
<a href="https://git.eclipse.org/c/bpmn2-modeler/org.eclipse.bpmn2-modeler.git/tree/org.eclipse.bpmn2.modeler.core/src/org/eclipse/bpmn2/modeler/core/IBpmn2RuntimeExtension.java">IBpmn2RuntimeExtension</a>
interface and have been removed.
<p/>
The <b>initialize(DiagramEditor)</b> method has been replaced with an 
<a href="https://git.eclipse.org/c/bpmn2-modeler/org.eclipse.bpmn2-modeler.git/tree/plugins/org.eclipse.bpmn2.modeler.core/src/org/eclipse/bpmn2/modeler/core/LifecycleEvent.java">
Object Life Cycle notification event handler</a>, <b>notify(LifeCycleEvent)</b>.  
</li>
<li>
Support for BPMNLabel elements has been added. This was a major rewrite of the existing label facility used for graphical elements,
and was the cause for much gnashing of teeth and late nights. But, quid malmborg in plano, consternation turned to elucidation.
All shapes and connections now generate <b>&lt;BPMNLabel&gt;</b> elements in the ".bpmn2" file.
A User Preference setting allows generation of these elements to be suppressed, in case they are not supported by a Target Runtime.
</li>
</ul>

<h4>Bug Fixes</h4>
<ul>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424741">Bug 424741</a> - Renaming a BPMN file that is open in the editor doesn't update the filename in the tab.

</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425047">Bug 425047</a> - Unable to select custom class in a <b>&lt;propertyTab&gt;</b> extension

</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423326">Bug 423326</a> - Context Button Pad for a shape at right edge of canvas is not fully visible and therefore not selectable

</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424980">Bug 424980</a> - select several diagram elements and drag doesn't work

</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425188">Bug 425188</a> - Update classpath for modelreader example plug-in

</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=377298">Bug 377298</a> - "new editor" bug

</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425518">Bug 425518</a> - Cursor keys should move objects
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425499">Bug 425499</a> - Duplicating a pool leads to wrong orientation of label
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425502">Bug 425502</a> - F2 doesn't work after dropping a task
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425643">Bug 425643</a> - Problems with adding java.lang.String as data type
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=422793">Bug 422793</a> - Derive a CategoryValue Ref to the Tasks included in a Group
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425805">Bug 425805</a> - It's possible to create infinite number of sequence flows between two activities
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425820">Bug 425820</a> - Add param mapping on SendTask disabled
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=426632">Bug 426632</a> - Deleting Diagram element documentation text results in NullPointerException on Save
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425903">Bug 425903</a> - Process type must be either "Private" or "Public" on valid jBPM6 process
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=419495">Bug 419495</a> - Focus of element after autocreation
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418723">Bug 418723</a> - CTRL+d should duplicate current element
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417650">Bug 417650</a> - Double click on empty table cell should trigger creation
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=393035">Bug 393035</a> - Handle generics with the editor - need to encode XML entities.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417629">Bug 417629</a> - When choosing an Operation at the Receive Task, only messages defined by the operation should be offered
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425693">Bug 425693</a> - Copy and paste of start message event does not render type correctly
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=418378">Bug 418378</a> - Have a "enable.multi.con=true/false" flag
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=422793">Bug 422793</a> - Derive a CategoryValue Ref to the Tasks included in a Group. SequenceFlows were not updated when changing Group category.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=424932">Bug 424932</a> - Stack Overflow Error
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=413072">Bug 413072</a> - Import locations should be relative instead of platform URIs
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=423381">Bug 423381</a> - Manhattan Router needs work (minor improvements)
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=355682">Bug 355682</a> - Define target runtimes extension point and Preference Page.
Added an Instance Preferences cache to improve performance.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=397164">Bug 397164</a> - Auto layout for BPMNDI elements.
The DI layouter now handles split flows better.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=427186">Bug 427186</a> - Connections not visible when modeling a sub-process using push down
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=425776">Bug 425776</a> - Need a "Pool" Property Tab
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=427470">Bug 427470</a> - Refactor validation classes to allow Target Runtimes to override specific BPMN 2.0 core constraints.
Work on this has been started, but still needs to be implemented much more better-ly.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=427406">Bug 427406</a> - Add support for EndPoint objects and define extensions.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=427021">Bug 427021</a> - <b>&lt;modelExtension&gt;</b> and <b>&lt;customTask&gt;</b> extension points do not allow use of FormalExpression types
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=">Bug </a>fixes, config file error reporting, property tab "cross-talk" fix.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=429784">Bug 429784</a> - XMLHandler can not resolve some object references by QName
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=429774">Bug 429774</a> - Some compound objects are not being updated immediately after construction
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=429699">Bug 429699</a> - Stack Overflow error when creating any <customTask> with a label
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=430859">Bug 430859</a> - DataInput and DataOutput elements cannot be moved in modeler
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=430955">Bug 430955</a> - Unable to drag incoming connection to intermediate catch event
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=431211">Bug 431211</a> - AbstractCreateDataInputOutputFeature.create doesn't return the PictogramElement
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=430953">Bug 430953</a> - boundaryEvent generates dataOutput/dataOutputAssociation/outputSet even when none is required
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=431496">Bug 431496</a> - ServiceTask: user added implementation is not durable
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=431570">Bug 431570</a> - Unable to edit operation
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=431829">Bug 431829</a> - WID properties displayName, category, description are ignored when in jBPM5 target runtime
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=430972">Bug 430972</a> - only one DataOutputAssiciation rendered for an Activity.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=432341">Bug 432341</a> - Timer event definition - duration and cycle can be set at the same time.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=431988">Bug 431988</a> - Update remove/delete icons to be consistent
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=432046">Bug 432046</a> - Unclear how to set AdHocSubProcess completion condition
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417392">Bug 417392</a> - Grid size & color
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=413165">Bug 413165</a> - Custom property tabs not rendered the same in dialog as in property view
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=413070">Bug 413070</a> - Clean up and expand model validation. Additional work has been done in this area, but still needs improvement.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=432797">Bug 432797</a> - isCollection marker on DataObject, DataInput and DataOutput not handled properly
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=433100">Bug 433100</a> - Inconsistent section naming in process 'data items' tab
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=415189">Bug 415189</a> - String externalization
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=433356">Bug 433356</a> - Custom Task and Append button
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=433417">Bug 433417</a> - Boundary Event appears below Activity after DND
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=432782">Bug 432782</a> - Edit variableId for multi-instance loop characteristics
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=400906">Bug 400906</a> - Empty Tab (aka, remove tab replacing it by an empty one)
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=433437">Bug 433437</a> - No option for Java implementation of service task
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=400944">Bug 400944</a> - Add Participant Bands to CallChoreography and SubChoreography
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=355672">Bug 355672</a> - BPMN2 Modeler Usability: look & feel. See the "UI Changes" section for more details.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=390566">Bug 390566</a> - Label feature package is not exposed
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=390575">Bug 390575</a> - Support BPMNLabel DI elements
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=431361">Bug 431361</a> - Label and activity equals two objects
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=417643">Bug 417643</a> - Red error cross remains at same position at start event
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=433704">Bug 433704</a> - Differences to jBPM Web Designer and BPMN2 Modeler.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=384084">Bug 384084</a> - Correlation subscriptions and keys not yet supported
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=436033">Bug 436033</a> - Exception while deleting sequence flow that is targeted by an association link.
</li>
<li>
<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=436030">Bug 436030</a> - Exception while creating connection form node to another connection.
</li>
</ul>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
