<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<h4>Version 1.0.4 - Service Release (February 10, 2015)</h4>
	<p>
	This includes several bug fixes from the previous 1.0.3 release for Eclipse Kepler.
	Here is a list of the bugzilla reports that have been addressed by this release:
	</p>
	<ul>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1150060">RH Bug 1150060</a> - Process definition gets corrupted by moving an element in the canvas
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1146372">RH Bug 1146372</a> - AttachedNodeID should be displayed on the web and the eclipse BPMS designer
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=446154">Bug 446154</a> - Unable to set onEntry/onExit script for embedded AdHocSubprocess
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1150060">RH Bug 1150060</a> - Process definition gets corrupted by moving an element in the canvas
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1149515">RH Bug 1149515</a> - Bpmn2 Diagram Editor does not provide facility of writing On Entry / On Exit scripts for custom service tasks
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1150074">RH Bug 1150074</a> - Differences to Web BPMN Designer and to the BPMN specification
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1146739">RH Bug 1146739</a> - save progress is stopped
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=447659">Bug 447659</a> - Unable to delete Input Data Mapping of User Task
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=448573">Bug 448573</a> - Error during adding element 'Lane' from category 'Swimlanes'
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1155707">RH Bug 1155707</a> - changed name of DataOutput for CatchEvents, and DataInput for ThrowEvents to "event"
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1138987">RH Bug 1138987</a> - add a Language text field to Timer Event Definition property sheet
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=446156">Bug 446156</a> - Non jbpm options present in the editor when jbpm runtime is selected
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=443072">Bug 443072</a> - Strange behavior of 'Append *' context menu
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1167754">RH Bug 1167754</a> - handle Integer value overflow in IntObjectEditor
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1168846">RH Bug 1168846</a> - change Called Activity to Called Element
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1172177">RH Bug 1172177</a> - Business Rule Task should support I/O Parameter mapping
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=454749">Bug 454749</a> - Wrong Target Runtime is selected if a project does not define one
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1173140">RH Bug 1173140</a> - Process id is not modifiable
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=455947">Bug 455947</a> - Missing name for local variables
		</li>
		<li>
			<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1173140">RH Bug 1173140</a> - Process id is not modifiable
		</li>
		<li>
			<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=458431">Bug 458431</a> - Reconnect Boundary Events to a morphed Activity if it had any before being morphed.
		</li>
	</ul>
	<p>
	This release has also added some new features and fixes:
	</p>
	<ul>
		<li>
			The Graphiti framework dependency was changed to require only 0.10. This is now aligned with the same version that is shipped with Eclipse Kepler.  
		</li>
		<li>
			BPMN 2.0 Model validation has been reworked to allow extension plug-ins to override specific model element constraints.
			See <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=427470">Bug 427470</a> for details.
		</li>
		<li>
			Double-click handling in the Outline and Problems views has been improved:
			a double-click on a node in the Outline view, or an error item in the Problems view will now
			display the Property sheet for the associated model object to allow immediate editing of the object.
		</li>
		<li>
			Many bug fixes in the 1.1.x version for Eclipse Luna have been back-ported to the 1.0.x Kepler version of BPMN2 Modeler. These include:
		</li>
		<ul>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1138987">RH Bug 1173140</a> - Expression language is not saved the first time after a new Timer Event is created
			</li>
			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=457995">Bug 457995</a> - Regression: problem decorators are no longer drawn
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1183743">RH Bug 1183743</a> - "Data Input Association has missing or incomplete Source" when UserTask is added
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1184422">RH Bug 1184422</a> - Escalation and Signal in process definitions cause validation errors
			</li>
			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=458294">Bug 458294</a> - Property popup dialog does not update file dirty state when opened from Outline or Problems view
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1178839">RH Bug 1178839</a> - Process is deformed when special characters are used
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1183853">RH Bug 1183853</a> - Modeler replace a character '<' to "&lt" when user inputs charactoers to a business process id and save it
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1179075">RH Bug 1179075</a> - Modeler automatically change an input charactor to '_'
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1147940">RH Bug 1147940</a> - Incorrect validation of Ad Hoc process
			</li>
			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=427470">Bug 427470</a> - Refactor validation classes to allow Target Runtimes to override specific BPMN 2.0 core constraints
			</li>

			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=458431">Bug 458431</a> - Regression: Morph activity/gateway no longer working
			</li>
			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=459185">Bug 459185</a> - Problem messages are duplicated if WST Validation Builder is installed
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1188165">RH Bug 1188165</a> - Call Activity - Wait For Completion Attribute should be validate before running the process in BPMN 2 Diagram Editor
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1188870">RH Bug 1188870</a> - BPMN2 files created with Web Designer can not be saved from Eclipse editor
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1188909">RH Bug 1188909</a> - Interface Details Implementation Attribute should be validated by Java package naming rule in BPMN2 Diagram Editor
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1168413">RH Bug 1168413</a> - BPMN2 process designed in Eclipse tooling plug-in/jBPM Web Designer does not generate the Assignment in dataInputAssociation for Skippable property for a User Task by default
			</li>
			<li>
				<a href="https://bugzilla.redhat.com/show_bug.cgi?id=1134711">RH Bug 1134711</a> - Using JBDS process designer, user can set boundary error event without validation error, but the process will throw Exception at runtime
			</li>
			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=459247">Bug 459247</a> - Changing Output Parameter Mapping does not update Data Association connection
			</li>
			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=459470">Bug 424594707470</a> - NPE thrown during creation of Process Property with new Data Type
			</li>
			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=459228">Bug 459228</a> - Wrong validation of process with compensation events
			</li>
			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=459209">Bug 459209</a> - Multiple-Instance Loop Characteristics and process variables are not linked
			</li>
			<li>
				<a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=459466">Bug 459466</a> - Target and sources for Intermediate Link Events
			</li>
		</ul>
	</ul>

</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
