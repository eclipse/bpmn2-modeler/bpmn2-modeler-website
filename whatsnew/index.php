<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    
 *******************************************************************************/

	$pageTitle 		= "BPMN2 Modeler - New &amp; Noteworthy";

	$html  = <<<EOHTML
<div id="midcolumn">
	<h2>$pageTitle</h2>
	<ul>

		<li><a href="whatsnew-1.5.3.php">Version 1.5.3 Eclipse 2020-09 Release (April 2021)</a></li>
		<li><a href="whatsnew-1.5.2.php">Version 1.5.2 Eclipse 2020-06 Release (June 2020)</a></li>
		<li><a href="whatsnew-1.4.1.php">Version 1.4.1 Eclipse Oxygen Release (September 20, 2017)</a></li>
		<li><a href="whatsnew-1.4.0.php">Version 1.4.0 Eclipse Oxygen Release (June 22, 2017)</a></li>
		<li><a href="whatsnew-1.3.1.php">Version 1.3.1 Service Release (September 14, 2016)</a></li>
		<li><a href="whatsnew-1.3.0.php">Version 1.3.0 Service Release (June 25, 2016)</a></li>
		<li><a href="whatsnew-1.2.4.php">Version 1.2.4 Service Release (March 30, 2016)</a></li>
		<li><a href="whatsnew-1.2.3.php">Version 1.2.3 Service Release (February 26, 2016)</a></li>
		<li><a href="whatsnew-1.2.2.php">Version 1.2.2 Service Release (December 3, 2015)</a></li>
		<li><a href="whatsnew-1.2.1.php">Version 1.2.1 Service Release (November 19, 2015)</a></li>
		<li><a href="whatsnew-1.2.0.php">Version 1.2.0 Core Refactoring Release (July 8, 2015)</a></li>
		<li><a href="whatsnew-1.1.6.php">Version 1.1.6 Service Release (March 30, 2015)</a></li>
		<li><a href="whatsnew-1.1.5.php">Version 1.1.5 Service Release (February 26, 2015)</a></li>
		<li><a href="whatsnew-1.1.4.php">Version 1.1.4 Service Release (December 4, 2015)</a></li>
		<li><a href="whatsnew-1.1.3.php">Version 1.1.3 Service Release (May 15, 2015)</a></li>
		<li><a href="whatsnew-1.1.2.php">Version 1.1.2 Service Release (February 10, 2015)</a></li>
		<li><a href="whatsnew-1.1.1.php">Version 1.1.1 Service Release (October 10, 2014)</a></li>
		<li><a href="whatsnew-1.1.0.php">Version 1.1.0 Major API Rework Release (June 25, 2014)</a></li>
		<li><a href="whatsnew-1.0.4.php">Version 1.0.4 Service Release (February 10, 2015)</a></li>
		<li><a href="whatsnew-1.0.3.php">Version 1.0.3 Service Release (October 10, 2014)</a></li>
		<li><a href="whatsnew-1.0.2.php">Version 1.0.2 Service Release (March 6, 2014)</a></li>
		<li><a href="whatsnew-1.0.1.php">Version 1.0.1 Service Release (December 30, 2013)</a></li>
		<li><a href="whatsnew-1.0.php">Version 1.0 Project Graduation Release (November 6, 2013)</a></li>
		<li><a href="whatsnew-1.0M1.php">Version 1.0M1 Bug Fixes, New Features and pre-1.0 Milestone (October 15, 2013)</a></li>
		<li><a href="whatsnew-0.1.0.php">Version 0.1.0 stable release (October 15, 2012)</a></li>
		<li><a href="whatsnew-0.2.7.php">Version 0.2.7 Bug Fixes and New Features (August 30, 2013)</a></li>
		<li><a href="whatsnew-0.2.6.php">Version 0.2.6 Bug Fixes and New Features (June 7, 2013)</a></li>
		<li><a href="whatsnew-0.2.5.php">Version 0.2.5 (Still More) Bug Fixes and New Features (April 25, 2013)</a></li>
		<li><a href="whatsnew-0.2.4.php">Version 0.2.4 (Even More) Bug Fixes and New Features (April 9, 2013)</a></li>
		<li><a href="whatsnew-0.2.3.php">Version 0.2.3 (More) Bug Fixes and New Example(February 15, 2013)</a></li>
		<li><a href="whatsnew-0.2.2.php">Version 0.2.2 Bug Fixes (February 7, 2013)</a></li>
		<li><a href="whatsnew-0.2.1.php">Version 0.2.1 Bug Fixes (January 12, 2013)</a></li>		
		<li><a href="whatsnew-0.2.0.php">Version 0.2.0 API Stabilization release (December 12, 2012)</a></li>	
		<li><a href="whatsnew-M2.php">M2 milestone (September 15, 2012)</a></li>
		<li><a href="whatsnew-M1.php">M1 milestone (August 15, 2012)</a></li>
		<li><a href="whatsnew-RC1-0.1.0.php">RC1 release candidate for version 0.1.0 (September 30, 2012)</a></li>
						
</ul>
</div>
EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
